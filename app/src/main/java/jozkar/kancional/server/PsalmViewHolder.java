package jozkar.kancional.server;

import android.content.ClipData;
import android.content.Context;
import android.content.Intent;
import android.preference.PreferenceManager;
import android.util.Log;
import android.view.View;
import android.view.inputmethod.InputMethodManager;
import android.widget.TextView;
import android.widget.Toast;

import androidx.appcompat.app.AlertDialog;
import androidx.appcompat.view.ContextThemeWrapper;
import androidx.fragment.app.FragmentActivity;
import androidx.recyclerview.widget.RecyclerView;

import com.google.android.material.dialog.MaterialAlertDialogBuilder;
import com.google.android.material.snackbar.Snackbar;

import org.json.JSONObject;

import java.net.ConnectException;
import java.util.ArrayList;

/**
 * Created by Josef Ridky on 14-07-2019.
 */
public class PsalmViewHolder extends RecyclerView.ViewHolder implements View.OnClickListener, View.OnLongClickListener {

    public TextView text;
    public FragmentActivity context;
    public boolean dialog;

    public PsalmViewHolder(View itemView, FragmentActivity c, boolean dialog) {
        super(itemView);

        //implementing onClickListener
        itemView.setOnClickListener(this);
        itemView.setOnLongClickListener(this);
        text = (TextView) itemView.findViewById(R.id.text);
        this.dialog = dialog;
        context = c;
    }

    @Override
    public void onClick(View view) {
        //Every time you click on the row toast is displayed

        try {
            if(dialog){
                RemoteFragment.dialogSearch.setQuery(text.getText(), false);
                InputMethodManager imm = (InputMethodManager) context.getSystemService(Context.INPUT_METHOD_SERVICE);
                if (imm != null && imm.isActive()) {
                    imm.hideSoftInputFromWindow(view.getWindowToken(), 0);
                }
            } else {

                Long tsLong = System.currentTimeMillis() / 1000;

                MainScreen.asyncNumber = "";
                MainScreen.asyncVerse = "";
                MainScreen.asyncPsalm = text.getText().toString();
                MainScreen.asyncBalik = "";
                MainScreen.asyncDodatek = "";
                MainScreen.asyncTime = tsLong.toString();
                App.lastPsalm = text.getText().toString();
                App.SP.edit().putString("lastPsalm", App.lastPsalm).apply();


                if (MainScreen.audic) {
                    MainScreen.IP = PreferenceManager.getDefaultSharedPreferences(context).getString("dialIP", "");
                    MainScreen.KEY = PreferenceManager.getDefaultSharedPreferences(context).getString("dialKey", "");
                    new MainScreen.AsyncPOST().execute();
                }

                if (MainScreen.CODE.equals("")) {
                    showAlert();
                }

                send();
            }
        } catch (Exception e) {
            Intent k = new Intent(context, Splash.class);
            context.startActivity(k);
        }
    }

    @Override
    public boolean onLongClick(View view) {
        try {
            ClipData clip = ClipData.newPlainText("name", text.getText());
            App.clipboard.setPrimaryClip(clip);
            Toast.makeText(context, context.getString(R.string.clipboard), Toast.LENGTH_SHORT).show();
        } catch (Exception e) {
            Intent k = new Intent(context, Splash.class);
            context.startActivity(k);
        }
        return true;
    }

    private void showAlert() {
        MaterialAlertDialogBuilder dlgAlert = new MaterialAlertDialogBuilder(context);
        float dpi = context.getResources().getDisplayMetrics().density;
        TextView m = new TextView(context);
        m.setText(context.getString(R.string.wrongCode));
        m.setPadding((int) (25 * dpi), (int) (19 * dpi), (int) (25 * dpi), (int) (14 * dpi));
        dlgAlert.setView(m);
        dlgAlert.setTitle(context.getString(R.string.wrongCodeTitle));
        dlgAlert.setCancelable(false);
        dlgAlert.setPositiveButton(R.string.ok, null);
        dlgAlert.create().show();
    }

    public void showError() {
        try {
            final MaterialAlertDialogBuilder alertBox = new MaterialAlertDialogBuilder(context);
            alertBox.setTitle(context.getString(R.string.wifi));
            alertBox.setMessage(context.getString(R.string.unreachable));
            alertBox.setCancelable(false)
                    .setPositiveButton("Ok", null).create().show();
        } catch (Exception e) {
            Log.d("ERROR", e.getLocalizedMessage());
        }
    }

    public void send() {
        try {
            if (App.ws.isOpen()) {
                JSONObject data = new JSONObject();
                if (!MainScreen.asyncBalik.isEmpty() || !MainScreen.asyncDodatek.isEmpty() || !MainScreen.asyncNumber.isEmpty() || !MainScreen.asyncPsalm.isEmpty()
                        || !MainScreen.asyncVerse.isEmpty() || !MainScreen.asyncTime.isEmpty()) {
                    data.put("time", MainScreen.asyncTime);
                    data.put("source", MainScreen.asyncDodatek);
                    data.put("package", MainScreen.asyncBalik);
                    data.put("song", MainScreen.asyncNumber);
                    data.put("psalm", MainScreen.asyncPsalm);
                    data.put("verse", MainScreen.asyncVerse);
                    data.put("crc", MainScreen.hash(MainScreen.LOCALIP + "|" + MainScreen.CODE));

                    if (MainScreen.camera) {
                        data.put("tv", MainScreen.cameraActive);
                    } else {
                        data.put("tv", true);
                        if (!MainScreen.cameraActive) {
                            MainScreen.cameraActive = true;
                            App.SP.edit().putBoolean("cameraActive", true).apply();
                        }
                    }
                }
                Log.d("JSON", data.toString() + MainScreen.LOCALIP);
                App.ws.sendText(data.toString());
                RemoteFragment.search.setQuery("", false);
            } else {
                throw new ConnectException("not connected");
            }
            Snackbar.make(text, context.getString(R.string.psalmResult), 1500).show();
            PreferenceManager.getDefaultSharedPreferences(context).edit().putString("currentNumber", "").putString("currentSloka", "").putBoolean("psalm", true).apply();
        } catch (ConnectException e) {
            showError();
        } catch (Exception e) {
            e.printStackTrace();
        }
    }
}