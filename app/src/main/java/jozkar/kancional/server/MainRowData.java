package jozkar.kancional.server;

/**
 * Created by Jozkar on 14.11.2015.
 */
public class MainRowData {

    public String name, id;
    public int icon;
    public int position;


    public MainRowData(int position, String name, int icon) {
        this.position = position;
        this.name = name;
        this.icon = icon;
    }
}
