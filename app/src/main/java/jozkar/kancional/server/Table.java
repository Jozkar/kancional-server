package jozkar.kancional.server;

import android.os.Bundle;

import androidx.appcompat.widget.Toolbar;
import androidx.viewpager.widget.ViewPager;

import com.google.android.material.tabs.TabLayout;

public class Table extends AppCompatWrapper {

    Toolbar toolbar;
    TabLayout tabLayout;
    ViewPager viewPager;
    PagerAdapter pagerAdapter;
    public static int type, noResults;
    public static String search, sloka;

    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);

        setContentView(R.layout.activitiy_table);

        type = getIntent().getIntExtra("type", 0);
        sloka = getIntent().getStringExtra("sloka");
        search = getIntent().getStringExtra("search");
        noResults = getIntent().getIntExtra("noResults", 0);

        if (sloka == null) {
            sloka = "0";
        }

        pagerAdapter = new PagerAdapter(getSupportFragmentManager(), getApplicationContext());
        //initializing toolbar
        toolbar = (Toolbar) findViewById(R.id.toolbar);
        switch (type) {
            case App.byNumber:
                toolbar.setTitle(R.string.chooseNumber);
                break;
            case App.byName:
                toolbar.setTitle(R.string.chooseLetter);
                break;
            case App.byGroup:
                toolbar.setTitle(R.string.chooseGroup);
                break;
            case App.searchResultNumber:
            case App.searchResultString:
                toolbar.setTitle(R.string.search);
                break;
            case App.plugin:
                toolbar.setTitle(R.string.action_plugins);
                break;
        }
        setSupportActionBar(toolbar);
        getSupportActionBar().setDisplayHomeAsUpEnabled(true);

        //initializing tab layout
        tabLayout = (TabLayout) findViewById(R.id.tab_layout);
        //initializing ViewPager
        viewPager = (ViewPager) findViewById(R.id.view_pager);

        viewPager.setAdapter(pagerAdapter);
        tabLayout.setTabsFromPagerAdapter(pagerAdapter);

        //giving viewPager reference to tablayout so that the viewPager changes when tab is clicked
        tabLayout.setupWithViewPager(viewPager);

        //giving tablayout reference to viewPager so that the tablayout changes when viewPager is scrolled
        viewPager.addOnPageChangeListener(new TabLayout.TabLayoutOnPageChangeListener(tabLayout));

    }
}

