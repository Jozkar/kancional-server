package jozkar.kancional.server;

import android.os.Bundle;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.widget.Toast;

import androidx.fragment.app.Fragment;
import androidx.viewpager.widget.ViewPager;

import com.google.android.material.tabs.TabLayout;

public class SetupFragment extends Fragment {

    View view;
    TabLayout tabLayout;
    ViewPager viewPager;
    RemotePagerAdapter pagerAdapter;
    public static int type;
    public static String search, sloka;

    @Override
    public View onCreateView(LayoutInflater inflater, ViewGroup container, Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);

        view = inflater.inflate(R.layout.fragment_remote, container, false);


        pagerAdapter = new RemotePagerAdapter(getFragmentManager(), getContext());
        //initializing toolbar

        //initializing tab layout
        tabLayout = (TabLayout) view.findViewById(R.id.tab_layout);
        //initializing ViewPager
        viewPager = (ViewPager) view.findViewById(R.id.view_pager);

        viewPager.setAdapter(pagerAdapter);
        tabLayout.setTabsFromPagerAdapter(pagerAdapter);

        //giving viewPager reference to tablayout so that the viewPager changes when tab is clicked
        tabLayout.setupWithViewPager(viewPager);

        //giving tablayout reference to viewPager so that the tablayout changes when viewPager is scrolled
        viewPager.addOnPageChangeListener(new TabLayout.TabLayoutOnPageChangeListener(tabLayout));
        viewPager.addOnPageChangeListener(new ViewPager.SimpleOnPageChangeListener(){
            @Override
            public void onPageSelected(int position){
                if(position > 1) {
                    Toast.makeText(getContext(), R.string.dragndrop, Toast.LENGTH_LONG).show();
                }
            }
        });
        return view;
    }
}

