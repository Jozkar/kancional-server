package jozkar.kancional.server;

import android.app.Application;
import android.content.ClipboardManager;
import android.content.Context;
import android.content.SharedPreferences;
import android.content.pm.PackageManager;
import android.net.nsd.NsdManager;
import android.preference.PreferenceManager;


import com.neovisionaries.ws.client.WebSocket;

import java.io.BufferedReader;
import java.io.IOException;
import java.io.InputStream;
import java.io.InputStreamReader;
import java.util.ArrayList;
import java.util.Collections;
import java.util.regex.Pattern;

public class App extends Application {
    public static NsdManager nsdManager;

    public static SongDb songs;
    public static DBHelper mydb;
    public static ArrayList<String> psalms;
    public static ArrayList<PlaylistRecord> playlist;
    public static final int byNumber = 0, byName = 1, byGroup = 2, byTypeNumber = 6, searchResultNumber = 3, searchResultString = 4, plugin = 5;
    public static int plPosition;
    public static Pattern pattern;
    public static String[] sharedPackages;
    public static boolean[] plugins;
    public static SharedPreferences SP;
    public static WebSocket ws = null;
    public static KancionalSearch ks;
    public static String lastPsalm;
    public static ClipboardManager clipboard;


    @Override
    public void onCreate(){
        super.onCreate();
        SP = PreferenceManager.getDefaultSharedPreferences(getBaseContext());
        ks = new KancionalSearch();
        mydb = new DBHelper(this);
        plPosition = SP.getInt("playlist",-1);
        pattern = Pattern.compile("[\\p{InCombiningDiacriticalMarks}]+");
        sharedPackages = getResources().getStringArray(R.array.plugins);
        lastPsalm = SP.getString("lastPsalm", "");
        plugins = new boolean[sharedPackages.length];
        for (int i = 0; i < sharedPackages.length; i++) {
            plugins[i] = isPackageInstalled(sharedPackages[i], getPackageManager());
        }

        clipboard = (ClipboardManager) getSystemService(Context.CLIPBOARD_SERVICE);

        loadSongs();
        loadPsalms();

        playlist = mydb.getRows();
    }

    private void loadSongs() {
        InputStream input = getResources().openRawResource(R.raw.database);
        BufferedReader reader = new BufferedReader(new InputStreamReader(input));
        String line = null;
        songs = new SongDb();
        try {
            while((line = reader.readLine()) != null){
                String [] val = line.split("::");
                songs.add(new Record(val[0],val[1],val[2],val[3],val[4],val[5],val[6],val[7],val[8],getPackageName().replace(".server",""),""));
            }
        } catch (IOException e) {
            e.printStackTrace();
        }

        for(int i = 0; i< sharedPackages.length; i++){
            if(plugins[i]){
                try {
                    Context friend = createPackageContext(sharedPackages[i], CONTEXT_IGNORE_SECURITY);
                    input = friend.getResources().openRawResource(friend.getResources().getIdentifier("database","raw",sharedPackages[i]));
                    String name = friend.getResources().getString(friend.getResources().getIdentifier("app_name","string",sharedPackages[i]));
                    reader = new BufferedReader(new InputStreamReader(input));
                    while((line = reader.readLine()) != null){
                        String [] val = line.split("::");
                        songs.add(new Record(val[0],val[1],val[2],val[3],val[4],val[5],val[6],val[7],val[8],sharedPackages[i], name));
                    }
                } catch (Exception e) {
                    e.printStackTrace();
                }
            }
        }
    }

    private void loadPsalms() {
        InputStream input = getResources().openRawResource(R.raw.psalms);
        BufferedReader reader = new BufferedReader(new InputStreamReader(input));
        String line = null;
        psalms = new ArrayList<String>();
        try {
            while((line = reader.readLine()) != null){
                psalms.add(line);
            }
        } catch (IOException e) {
            e.printStackTrace();
        }
        Collections.sort(psalms);
    }

    public static boolean isPackageInstalled(String packagename, PackageManager packageManager) {
        try {
            packageManager.getPackageInfo(packagename, 0);
            return true;
        } catch (PackageManager.NameNotFoundException e) {
            return false;
        }
    }

}
