package jozkar.kancional.server;

import android.graphics.Typeface;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;

import androidx.fragment.app.FragmentActivity;
import androidx.recyclerview.widget.RecyclerView;

import java.util.List;

public class PsalmViewAdapter extends RecyclerView.Adapter<PsalmViewHolder> {

    private List<String> itemList;
    private FragmentActivity context;
    private boolean dialog;

    public PsalmViewAdapter(FragmentActivity context, List<String> itemList) {
        this.itemList = itemList;
        this.context = context;
        this.dialog = false;
    }

    public PsalmViewAdapter(FragmentActivity context, List<String> itemList, boolean dialog) {
        this.itemList = itemList;
        this.context = context;
        this.dialog = dialog;
    }

    @Override
    public PsalmViewHolder onCreateViewHolder(ViewGroup parent, int viewType) {
        View view = LayoutInflater.from(parent.getContext()).inflate(R.layout.hint_row, null);
        return new PsalmViewHolder(view, context, dialog);
    }

    @Override
    public void onBindViewHolder(PsalmViewHolder holder, int position) {
        holder.text.setText(itemList.get(position));
        if (MainScreen.serif) {
            holder.text.setTypeface(Typeface.SERIF);
        }
    }

    @Override
    public int getItemCount() {
        return this.itemList.size();
    }

}

