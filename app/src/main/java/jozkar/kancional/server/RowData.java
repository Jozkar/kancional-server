package jozkar.kancional.server;

/**
 * Created by Jozkar on 10.11.2015.
 */
public class RowData {

    private String number;
    private String name;
    private String details;
    private String dodatek;
    private String plugin;
    private String balik;

    public RowData(String number, String name, String details, String dodatek, String plugin, String balik) {
        this.name = name;
        this.number = number;
        this.details = details;
        this.dodatek = dodatek;
        this.plugin = plugin;
        this.balik = balik;
    }

    public String getName() {

        return name;
    }

    public void setName(String name) {

        this.name = name;
    }

    public String getDescribe() {

        return details;
    }

    public String getDodatek() {

        return dodatek;
    }

    public String getNumber() {

        return number;
    }

    public void setNumber(String number) {

        this.number = number;
    }

    public String getPlugin() {
        return plugin;
    }

    public String getBalik() {
        return balik;
    }
}


