package jozkar.kancional.server;

import android.content.Context;
import android.os.Bundle;

import androidx.fragment.app.Fragment;
import androidx.fragment.app.FragmentManager;
import androidx.fragment.app.FragmentStatePagerAdapter;

import java.util.ArrayList;
import java.util.Collections;

class PagerAdapter extends FragmentStatePagerAdapter {
    Fragment fragment = null;
    Context c;

    public PagerAdapter(FragmentManager fm, Context context) {
        super(fm);
        c = context;
    }

    @Override
    public Fragment getItem(int position) {
        //Based upon the position you can call the fragment you need here
        //here i have called the same fragment for all the instances
        fragment = new MainFragment();
        Bundle args = new Bundle();
        args.putInt("id", position);
        args.putInt("type", Table.type);
        args.putString("search", Table.search);
        args.putString("sloka", Table.sloka);
        fragment.setArguments(args);
        return fragment;
    }


    @Override
    public int getCount() {
        // Returns the number of tabs (If you need 4 tabs change it to 4)
        switch (Table.type) {
            case App.byNumber:
                return 10;
            case App.byName:
                return 23;
            case App.byGroup:
                return 14;
            case App.searchResultNumber:
            case App.searchResultString:
                return 1;
            case App.plugin:
                int count = 0;
                for (int i = 0; i < App.plugins.length; i++) {
                    if (App.plugins[i]) {
                        count++;
                    }
                }
                return count == 0 ? 1 : count;
        }
        return 0;
    }

    @Override
    public CharSequence getPageTitle(int position) {
        //this is where you set texts to your tabs based upon the position
        //positions starts from 0
        String[] tabs;
        switch (Table.type) {
            case App.byNumber:
                tabs = c.getResources().getStringArray(R.array.fromNumber);
                break;
            case App.byName:
                tabs = c.getResources().getStringArray(R.array.letters);
                break;
            case App.byGroup:
                tabs = c.getResources().getStringArray(R.array.sections);
                break;
            case App.searchResultNumber:
            case App.searchResultString:
                tabs = new String[1];
                tabs[0] = String.format(c.getString(R.string.searchResult), Table.noResults);
                break;
            case App.plugin:
                ArrayList<String> plugins = new ArrayList<>();
                tabs = new String[1];
                Collections.addAll(plugins, c.getResources().getStringArray(R.array.pluginsTit));
                for (int i = 0, removed = 0; i < App.plugins.length; i++) {
                    if (!App.plugins[i]) {
                        plugins.remove(i - removed);
                        removed++;
                    }
                }
                tabs = plugins.toArray(tabs);
                break;
            default:
                tabs = new String[0];
        }
        if (tabs.length > position) {
            return tabs[position];
        } else {
            return "";
        }
    }
}