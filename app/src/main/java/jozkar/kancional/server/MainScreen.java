package jozkar.kancional.server;

import android.content.DialogInterface;
import android.content.Intent;
import android.content.res.Configuration;
import android.os.Bundle;
import android.provider.Settings;
import android.text.Html;
import android.text.Spanned;
import android.text.method.LinkMovementMethod;
import android.util.Log;
import android.view.LayoutInflater;
import android.view.Menu;
import android.view.MenuInflater;
import android.view.MenuItem;
import android.view.View;
import android.widget.EditText;
import android.widget.TextView;
import android.widget.Toast;

import androidx.annotation.NonNull;
import androidx.appcompat.widget.Toolbar;

import com.google.android.material.bottomnavigation.BottomNavigationView;
import com.google.android.material.dialog.MaterialAlertDialogBuilder;
import com.google.android.material.snackbar.Snackbar;
import com.neovisionaries.ws.client.WebSocket;
import com.neovisionaries.ws.client.WebSocketAdapter;
import com.neovisionaries.ws.client.WebSocketFactory;
import com.neovisionaries.ws.client.WebSocketState;

import org.json.JSONException;
import org.json.JSONObject;

import java.util.Timer;
import java.util.TimerTask;

import jozkar.kancional.server.NavigationHelper.Screen;

import static jozkar.kancional.server.App.SP;
import static jozkar.kancional.server.App.ks;
import static jozkar.kancional.server.App.ws;


public class MainScreen extends AppCompatWrapper {

    public static Toolbar toolbar;
    public static Screen current;
    public static String lastState = "";
    Timer lifeTimer;
    public static NavigationHelper navigationHelper;
    BottomNavigationView navigation;


    private BottomNavigationView.OnNavigationItemSelectedListener mOnNavigationItemSelectedListener
            = new BottomNavigationView.OnNavigationItemSelectedListener() {

        @Override
        public boolean onNavigationItemSelected(@NonNull MenuItem item) {
            if (current == Screen.SETTINGS) {
                onStart();
            }

            getSupportActionBar().show();
            switch (item.getItemId()) {
                case R.id.home:
                    getSupportActionBar().setTitle(R.string.app_name);
                    if(getResources().getConfiguration().orientation == Configuration.ORIENTATION_LANDSCAPE){
                        getSupportActionBar().hide();
                    }
                    navigationHelper.navigate(Screen.HOME);
                    current = Screen.HOME;
                    return true;
                case R.id.search:
                    getSupportActionBar().setTitle(R.string.search);
                    navigationHelper.navigate(Screen.SEARCH);
                    current = Screen.SEARCH;
                    return true;
                case R.id.list:
                    getSupportActionBar().setTitle(R.string.content);
                    navigationHelper.navigate(Screen.LISTS);
                    current = Screen.LISTS;
                    return true;
                case R.id.settings:
                    getSupportActionBar().setTitle(R.string.action_settings);
                    navigationHelper.navigate(Screen.SETTINGS);
                    current = Screen.SETTINGS;
                    return true;
                default:
                    getSupportActionBar().setTitle(R.string.other);
                    navigationHelper.navigate(Screen.OTHER);
                    current = Screen.OTHER;
                    return true;
            }
        }
    };


    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);

        if (!getIntent().getBooleanExtra("DHCP", true)) {
            MaterialAlertDialogBuilder alrt = getAlert(getString(R.string.wifiAddress), getString(R.string.wifi));
            alrt.setPositiveButton(R.string.ok, new DialogInterface.OnClickListener() {
                @Override
                public void onClick(DialogInterface dialog, int which) {
                    startActivity(new Intent(Settings.ACTION_WIFI_SETTINGS));
                }
            });
            alrt.create().show();
        }

        SP.edit().putString("currentNumber", "").apply();
        SP.edit().putBoolean("psalm", false).apply();
        SP.edit().putString("currentSloka", "").apply();



        if (CODE.equals("")) {

            MaterialAlertDialogBuilder dlgAlert = getAlert(getString(R.string.noCode), getString(R.string.code));

            LayoutInflater inflater = this.getLayoutInflater();
            View dialogView = inflater.inflate(R.layout.dialog_settings, null);

            dlgAlert.setView(dialogView);
            dlgAlert.setTitle(R.string.code);
            dlgAlert.setPositiveButton("OK", new DialogInterface.OnClickListener() {
                @Override
                public void onClick(DialogInterface dialogInterface, int i) {
                    CODE = ((EditText) dialogView.findViewById(R.id.code)).getText().toString();
                    SP.edit().putString("code", CODE).apply();
                }
            });
            dlgAlert.setCancelable(false);
            dlgAlert.create().show();
        }


        setContentView(R.layout.activity_main_screen);
        toolbar = (Toolbar) findViewById(R.id.toolbar);
        toolbar.setTitle(R.string.app_name);
        setSupportActionBar(toolbar);
        getSupportActionBar().setDisplayShowHomeEnabled(true);
        contactServer();

        navigation = (BottomNavigationView) findViewById(R.id.navigation);
        navigation.setOnNavigationItemSelectedListener(mOnNavigationItemSelectedListener);

        navigationHelper = new NavigationHelper(getSupportFragmentManager());
        navigationHelper.navigate(current);
        if (current != null) {
            switch (current) {
                case HOME:
                    navigation.setSelectedItemId(R.id.home);
                    break;
                case LISTS:
                    navigation.setSelectedItemId(R.id.list);
                    break;
                case SEARCH:
                    navigation.setSelectedItemId(R.id.search);
                    break;
                case SETTINGS:
                    navigation.setSelectedItemId(R.id.settings);
                    break;
                case OTHER:
                    navigation.setSelectedItemId(R.id.other);
                    break;
            }
        } else {
            if(getResources().getConfiguration().orientation == Configuration.ORIENTATION_LANDSCAPE){
                getSupportActionBar().hide();
            }
        }

        if (version < getResources().getInteger(R.integer.version)) {
            MaterialAlertDialogBuilder dlgAlert = new MaterialAlertDialogBuilder(this);
            float dpi = getResources().getDisplayMetrics().density;
            TextView m = new TextView(this);
            Spanned msg = Html.fromHtml(getString(R.string.about_app));
            m.setText(msg);
            m.setLinkTextColor(getResources().getColor(R.color.link));
            m.setMovementMethod(LinkMovementMethod.getInstance());
            m.setPadding((int) (25 * dpi), (int) (19 * dpi), (int) (25 * dpi), (int) (14 * dpi));
            dlgAlert.setView(m);
            dlgAlert.setTitle(R.string.news);
            dlgAlert.setPositiveButton("OK", new DialogInterface.OnClickListener() {
                @Override
                public void onClick(DialogInterface dialogInterface, int i) {
                }
            });
            dlgAlert.setCancelable(false);
            dlgAlert.create().show();
            SP.edit().putInt("oldVersion", getResources().getInteger(R.integer.version)).apply();
        }


    }

    @Override
    public boolean onOptionsItemSelected(MenuItem item) {
        // Handle action bar item clicks here. The action bar will
        // automatically handle clicks on the Home/Up button, so long
        // as you specify a parent activity in AndroidManifest.xml.
        int Id = item.getItemId();

        //noinspection SimplifiableIfStatement
        if (Id == R.id.currentNumber) {
            if (item.getTitle().equals(getString(R.string.menuTextPsalm))) {
                showError("psalm");
            }
        }

        return super.onOptionsItemSelected(item);
    }

    @Override
    protected void onStart() {
        super.onStart();
        boolean set = SP.getBoolean("night", false), ser = SP.getBoolean("serif", false), fs = SP.getBoolean("fullscreen", true), cam = SP.getBoolean("camera", false), num = SP.getBoolean("showNumber", false);
        secure = SP.getBoolean("secure", true);
        audic = SP.getBoolean("dial", false);
        allwaysVerse = SP.getBoolean("allwaysVerse", false);
        CODE = SP.getString("code", "");

        if (set != night || ser != serif || fs != fullscreen || cam != camera || num != showNumber) {
            Intent old = getIntent();
            finish();
            startActivity(old);
            super.onStart();
            return;
        }

        lifeTimer = new Timer();
        lifeTimer.schedule(new TimerTask() {
            @Override
            public void run() {
                if (ws == null || !ws.isOpen()) {
                    if (ws != null) {
                        ws.disconnect();
                    }
                    contactServer();
                }
            }
        }, 500, 2000);

    }

    public void contactServer() {
        WebSocketFactory fac = new WebSocketFactory();
        fac.setConnectionTimeout(1800);

        if (ws == null || !ws.isOpen()) {
            try {
                if (ks.IP.equals("") || ks.PORT.equals("")) {
                    ks.discovery();
                    SP.edit().putString("currentError", "close").apply();
                    invalidateOptionsMenu();
                    return;
                }

                if (LOCALIP == null || LOCALIP.isEmpty()) {
                    SP.edit().putString("currentError", "close").apply();
                    checkWifi();
                    invalidateOptionsMenu();
                    return;
                }

                ws = fac.createSocket("ws:/" + ks.IP + ":" + ks.PORT);

                ws.addHeader("origin", LOCALIP);

                ws.addListener(new WebSocketAdapter() {
                    @Override
                    public void onTextMessage(WebSocket webSocket, String message) {
                        Log.d("ZPRAVA", message);

                        if (message != null && message.equals("no")) {
                            runOnUiThread(new Runnable() {
                                @Override
                                public void run() {
                                    showError("no");
                                }
                            });
                        }
                        if (message != null && message.equals("ok")) {
                            return;
                        }
                        JSONObject data;
                        try {
                            data = new JSONObject(message);
                            SP.edit().putString("currentNumber", data.getString("song")).apply();
                            SP.edit().putBoolean("psalm", !data.getString("psalm").isEmpty()).apply();
                            SP.edit().putString("currentSloka", data.getString("verse")).apply();
                            SP.edit().putString("currentError", "").apply();
                            asyncBalik = data.getString("package");
                            asyncDodatek = data.getString("source");
                            asyncNumber = data.getString("song");
                            asyncPsalm = data.getString("psalm");
                            asyncVerse = data.getString("verse");
                            invalidateOptionsMenu();
                        } catch (JSONException e) {
                            SP.edit().putString("currentError", "off").apply();
                            e.printStackTrace();
                            SP.edit().putString("currentNumber", "").apply();
                            SP.edit().putBoolean("psalm", false).apply();
                            SP.edit().putString("currentSloka", "").apply();
                            asyncBalik = "";
                            asyncDodatek = "";
                            asyncNumber = "";
                            asyncPsalm = "";
                            asyncVerse = "";
                            invalidateOptionsMenu();
                        }
                    }
                });

                ws.addListener(new WebSocketAdapter() {
                    @Override
                    public void onStateChanged(WebSocket websocket, WebSocketState newState) {
                        Log.d("STATE", newState.name());
                        if (newState.name().equals("CLOSED")) {
                            SP.edit().putString("currentNumber", "").apply();
                            SP.edit().putBoolean("psalm", false).apply();
                            SP.edit().putString("currentSloka", "").apply();
                            SP.edit().putString("currentError", "close").apply();
                            invalidateOptionsMenu();
                            if (!MainScreen.lastState.isEmpty() && MainScreen.lastState.equals("OPEN")) {
                                runOnUiThread(new Runnable() {
                                    @Override
                                    public void run() {
                                        Snackbar.make(navigation, R.string.noConnection, 700).show();
                                    }
                                });
                                MainScreen.lastState = "CLOSED";
                            }
                            if(!ws.getURI().equals("ws:/" + ks.IP + ":" + ks.PORT)){
                                ws = null;
                                return;
                            }
                        }

                        if (newState.name().equals("OPEN")) {
                            if (MainScreen.lastState.isEmpty() || MainScreen.lastState.equals("CLOSED")) {
                                invalidateOptionsMenu();
                                runOnUiThread(new Runnable() {
                                    @Override
                                    public void run() {
                                        Snackbar.make(navigation, getString(R.string.availConnection), 700).show();
                                    }
                                });
                                MainScreen.lastState = "OPEN";
                            }
                        }
                    }
                });

                ws.connectAsynchronously();
            } catch (Exception e) {
                e.printStackTrace();
            }
        }
    }

    public void showError(String res) {
        if (res.equals("unreachable")) {
            MaterialAlertDialogBuilder alertBox = new MaterialAlertDialogBuilder(this);
            alertBox.setTitle(getString(R.string.wifi));
            alertBox.setMessage(getString(R.string.unreachable));
            alertBox.setCancelable(false)
                    .setPositiveButton("Ok", null).create().show();
            return;
        }

        if (res.equals("no")) {
            MaterialAlertDialogBuilder alertBox = new MaterialAlertDialogBuilder(MainScreen.this).setTitle(getString(R.string.wrongCodeTitle)).setMessage(getString(R.string.wrongCode));
            alertBox.setCancelable(false).setPositiveButton("Ok", null).create().show();
            MainScreen.CODE = "";
            return;
        }

        MaterialAlertDialogBuilder alertBox = new MaterialAlertDialogBuilder(MainScreen.this).setTitle(getString(R.string.menuTextPsalm)).setMessage(asyncPsalm);
        alertBox.setCancelable(false).setPositiveButton("Ok", null).create().show();
    }

    @Override
    protected void onStop() {
        if (lifeTimer != null) {
            lifeTimer.cancel();
        }

        ks.stop();
        super.onStop();
    }

    @Override
    protected void onDestroy() {
        if (ws != null) {
            ws.disconnect();
        }
        super.onDestroy();
    }

    @Override
    public boolean onPrepareOptionsMenu(Menu menu) {
        String last = SP.getString("currentNumber", "");
        String sloka = SP.getString("currentSloka", "");
        String reason = SP.getString("currentError", "close");
        boolean psa = SP.getBoolean("psalm", false);
        MenuInflater inflater = getMenuInflater();
        inflater.inflate(R.menu.number, menu);
        if (!last.equals("")) {
            menu.getItem(0).setTitle(String.format(getString(R.string.menuText), last + "/" + sloka));
        } else {
            if (psa) {
                menu.getItem(0).setTitle(getString(R.string.menuTextPsalm));
            } else {
                if (reason.equals("")) {
                    menu.getItem(0).setTitle(getString(R.string.menuTextEmpty));
                }
                if (reason.equals("off")) {
                    menu.getItem(0).setTitle(R.string.menuTextOff);
                }
                if (reason.equals("close")) {
                    menu.getItem(0).setTitle(R.string.menuTextDisconnect);
                }
            }
        }
        return true;
    }


    @Override
    public void onBackPressed() {
        if (current != null && current != Screen.HOME) {
            navigationHelper.navigate(Screen.HOME);
            navigation.setSelectedItemId(R.id.home);
            current = Screen.HOME;
            return;
        }

        MaterialAlertDialogBuilder dlgAlert = new MaterialAlertDialogBuilder(this);
        float dpi = getResources().getDisplayMetrics().density;
        TextView m = new TextView(this);
        m.setText(R.string.close_app);
        m.setPadding((int) (25 * dpi), (int) (19 * dpi), (int) (25 * dpi), (int) (14 * dpi));
        dlgAlert.setView(m);
        dlgAlert.setTitle(R.string.close_title);
        dlgAlert.setCancelable(false);
        dlgAlert.setPositiveButton(R.string.yes, new DialogInterface.OnClickListener() {
            @Override
            public void onClick(DialogInterface dialogInterface, int i) {
                finish();
            }
        });
        dlgAlert.setNegativeButton(R.string.no, new DialogInterface.OnClickListener() {
            @Override
            public void onClick(DialogInterface dialogInterface, int i) {

            }
        });
        dlgAlert.create().show();
    }
}
