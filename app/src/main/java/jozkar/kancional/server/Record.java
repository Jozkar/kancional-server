package jozkar.kancional.server;

import java.text.Normalizer;

/**
 * Created by Jozkar on 11.11.2015.
 */
public class Record {
    String id, name, text, skupina, dodatek, search, noty, soloText, balik, plugin;

    public Record(String id, String name, String skupina, String autor, String M, String dodatek, String text, String notes, String solotext, String balik, String plugin) {
        this.id = id;
        this.name = name;
        this.text = text;
        this.noty = notes;
        this.soloText = solotext;
        this.balik = balik;
        this.plugin = plugin;


        if (!M.equals("")) {
            this.text = "<strong>M <small>" + M + "</small></strong><br>" + text;
            this.soloText = "<strong>M <small>" + M + "</small></strong><br>" + soloText;
        }

        if (!autor.equals("")) {
            this.text = "<small>" + autor + "</small><br>" + this.text;
            this.soloText = "<small>" + autor + "</small><br>" + this.soloText;
        }

        if (!plugin.equals("")) {
            this.text = "<small><em>Zdroj: " + this.plugin + "</em></small><br>" + this.text;
            this.soloText = "<small><em>Zdroj: " + this.plugin + "</em></small><br>" + this.soloText;
        }

        if (!skupina.equals("")) {
            this.search = id + " " + Normalizer.normalize(this.name, Normalizer.Form.NFD) + " " + Normalizer.normalize(this.text, Normalizer.Form.NFD);
            this.search = App.pattern.matcher(this.search).replaceAll("").replaceAll(",", "").replaceAll(":","");
        }
        this.skupina = skupina;
        this.dodatek = dodatek;
    }
}
