package jozkar.kancional.server;

import java.text.Normalizer;

/**
 * Created by Jozkar on 11.11.2015.
 */
public class PlaylistRecord{
    String number;
    String verse;
    String psalm;
    String balik;
    String dodatek;

    public PlaylistRecord() {
        number = verse = psalm = balik = dodatek = "";
    }

    public String getNumber() {
        return number;
    }

    public void setNumber(String number) {
        this.number = number;
    }

    public String getVerse() {
        return verse;
    }

    public void setVerse(String verse) {
        this.verse = verse;
    }

    public String getPsalm() {
        return psalm;
    }

    public void setPsalm(String psalm) {
        this.psalm = psalm;
    }

    public String getBalik() {
        return balik;
    }

    public void setBalik(String balik) {
        this.balik = balik;
    }

    public String getDodatek() {
        return dodatek;
    }

    public void setDodatek(String dodatek) {
        this.dodatek = dodatek;
    }

}
