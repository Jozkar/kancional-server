package jozkar.kancional.server;

import android.content.Context;
import android.content.Intent;
import android.graphics.Typeface;
import android.os.Bundle;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.view.inputmethod.InputMethodManager;
import android.widget.EditText;
import android.widget.TextView;

import androidx.annotation.NonNull;
import androidx.fragment.app.Fragment;

import com.google.android.material.snackbar.Snackbar;

/**
 * Created by Jozkar on 29.11.2015.
 */
public class SearchFragment  extends Fragment {

    View view;
    private EditText edit;
    private InputMethodManager imm;

    public SearchFragment(){}

    @Override
    public View onCreateView(LayoutInflater inflater, ViewGroup container, Bundle savedInstanceState) {
        view = inflater.inflate(R.layout.activity_search, container, false);
        super.onCreate(savedInstanceState);

        TextView button = (TextView)view.findViewById(R.id.set);
        edit = (EditText)view.findViewById(R.id.search);
        button.setOnClickListener(this::onClick);

        imm = (InputMethodManager)getContext().getSystemService(Context.INPUT_METHOD_SERVICE);

        if(MainScreen.serif) {
            button.setTypeface(Typeface.SERIF);
            edit.setTypeface(Typeface.SERIF);
        }
        return view;
    }

    public void onClick(View v){
        String number = edit.getText().toString();

        try{

            int noResults = App.songs.checkSearch(number);
            if (imm.isActive()) {
                imm.hideSoftInputFromWindow(view.getWindowToken(), 0);

            }
            if(noResults > 0){
                Intent i = new Intent(getContext(), Table.class);
                i.putExtra("type", App.searchResultString);
                i.putExtra("search", number);
                i.putExtra("noResults", noResults);
                startActivity(i);
            }else{
                Snackbar snack = Snackbar.make(getActivity().findViewById(android.R.id.content), getString(R.string.wrongSearch), Snackbar.LENGTH_SHORT);
                snack.getView().setBackgroundColor(getResources().getColor(R.color.red));
                snack.show();
            }

        }catch(Exception e){
            Intent k = new Intent(getContext(), Splash.class);
            startActivity(k);
        }
        edit.setText(R.string.empty);
    }

    @Override
    public void onSaveInstanceState(@NonNull Bundle outState) {

    }
}
