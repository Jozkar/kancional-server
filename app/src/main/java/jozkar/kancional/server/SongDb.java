package jozkar.kancional.server;

import java.text.Normalizer;
import java.util.ArrayList;
import java.util.Collections;
import java.util.Comparator;

/**
 * Created by Jozkar on 11.11.2015.
 */
public class SongDb {

    ArrayList<Record> db;

    public SongDb() {
        db = new ArrayList<Record>();
    }

    public void add(Record r) {
        db.add(r);

    }

    public int getIndexByNumber(String number, String dodatek) {
        for (int i = 0; i < db.size(); i++) {
            if (db.get(i).id.equals(number) && db.get(i).dodatek.equals(dodatek)) {
                return i;
            }
        }
        return -1;
    }

    public boolean checkId(int id) {
        try {
            return (id >= 0 && !db.get(id).name.isEmpty());
        } catch (Exception e) {
            return false;
        }
    }

    public Record getByNumber(String number, String verze, String balik) {
        for (int i = 0; i < db.size(); i++) {
            if (db.get(i).id.equals(number) && db.get(i).dodatek.equals(verze) && db.get(i).balik.equals(balik)) {
                return db.get(i);
            }
        }
        return null;
    }

    public int getIndexByNumberFirstLetter(char letter) {
        for (int i = 0; i < db.size(); i++) {
            if (db.get(i).id.charAt(0) == letter) {
                return i;
            }
        }
        return -1;
    }

    public int getIndexByGroup(String name) {
        for (int i = 0; i < db.size(); i++) {
            if (db.get(i).skupina.equals(name)) {
                return i;
            }
        }
        return -1;
    }

    public int getIndexByPlugin(String name) {
        for (int i = 0; i < db.size(); i++) {
            if (db.get(i).balik.equals(name)) {
                return i;
            }
        }
        return -1;
    }

    public int getIndexByNameFirstLetter(char letter) {
        for (int i = 0; i < db.size(); i++) {
            if (db.get(i).name.charAt(0) == letter) {
                return i;
            }
        }
        return -1;
    }

    public String[] getNumbers(){
        ArrayList<String> retval = new ArrayList<>();
        for(Record r: db){
            if(!r.balik.equals("jozkar.kancional")){
                if(r.balik.endsWith("zpevnicek")){
                    retval.add(r.id + " (Zpěvníček)");
                }
                if(r.balik.endsWith("hlucin")){
                    retval.add(r.id + " (Hlučín)");
                }
                if(r.balik.endsWith("poboznosti")){
                    retval.add(r.id + " (Pobožnosti)");
                }
            } else {
                if (!r.dodatek.equals("")) {
                    retval.add(r.id + " (" + r.dodatek + ")");
                } else {
                    retval.add(r.id);
                }
            }
        }

        return retval.toArray(new String[0]);
    }

    public int checkNumber(String number) {
        int no = 0;
        for (int i = 0; i < db.size(); i++) {
            if (db.get(i).id.contains(number)) {
                no++;
            }
        }
        return no;
    }

    public Record getByNumber(String number) {
        for (int i = 0; i < db.size(); i++) {
            if (db.get(i).id.contains(number)) {
                return db.get(i);
            }
        }
        return null;
    }

    public int checkSearch(String number) {

        int no = 0;
        if (number.equals("")) {
            return no;
        }

        String normNumber = Normalizer.normalize(number, Normalizer.Form.NFD);
        normNumber = App.pattern.matcher(normNumber).replaceAll("");

        for (int i = 0; i < db.size(); i++) {
            if (db.get(i).search.matches("(?i:.*" + normNumber + ".*)")) {
                no++;
            }
        }
        return no;
    }

    public void sorting(int by) {

        switch (by) {
            case App.byName:
                Collections.sort(db, SongDb.CompareByName);
                break;
            case App.byNumber:
                Collections.sort(db, SongDb.CompareByNumber);
                break;
            case App.byGroup:
                Collections.sort(db, SongDb.CompareByGroupAndNumber);
                break;
            default:
                Collections.sort(db, SongDb.CompareByNumberAndPackage);
        }
    }

    public static Comparator<Record> CompareByNumber = new Comparator<Record>() {

        public int compare(Record r1, Record r2) {
            String number1 = r1.id;
            String number2 = r2.id;

            //ascending order
            return number1.compareTo(number2);

            //descending order
            //return number2.compareTo(number1);
        }
    };

    public static Comparator<Record> CompareByNumberAndPackage = new Comparator<Record>() {

        public int compare(Record r1, Record r2) {
            String number1 = r1.balik;
            String number2 = r2.balik;

            //ascending order
            int dateComparision = number1.compareTo(number2);
            return dateComparision == 0 ? r1.id.compareTo(r2.id) : dateComparision;
            //descending order
            //return number2.compareTo(number1);
        }
    };

    public static Comparator<Record> CompareByTypeAndNumber = new Comparator<Record>() {

        public int compare(Record r1, Record r2) {
            String number1 = r1.dodatek;
            String number2 = r2.dodatek;

            //ascending order
            int dateComparision = number1.compareTo(number2);
            return dateComparision == 0 ? r1.id.compareTo(r2.id) : dateComparision;
            //descending order
            //return number2.compareTo(number1);
        }
    };


    public static Comparator<Record> CompareByName = new Comparator<Record>() {

        public int compare(Record r1, Record r2) {
            String name1 = r1.name;
            String name2 = r2.name;

            //ascending order
            return name1.compareTo(name2);

            //descending order
            //return name2.compareTo(name1);
        }
    };

    public static Comparator<Record> CompareByGroupAndNumber = new Comparator<Record>() {

        public int compare(Record r1, Record r2) {
            String group1 = r1.skupina;
            String group2 = r2.skupina;
            int dateComparison = group1.compareTo(group2);
            return dateComparison == 0 ? r1.id.compareTo(r2.id) : dateComparison;
        }
    };
}
