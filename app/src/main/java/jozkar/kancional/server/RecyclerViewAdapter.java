package jozkar.kancional.server;

import android.graphics.Typeface;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;

import androidx.fragment.app.FragmentActivity;
import androidx.recyclerview.widget.RecyclerView;

import java.util.List;

public class RecyclerViewAdapter extends RecyclerView.Adapter<RecyclerViewHolder> {

    private List<RowData> itemList;
    private FragmentActivity context;
    private String sloka;

    public RecyclerViewAdapter(FragmentActivity context, List<RowData> itemList, String sloka) {
        this.itemList = itemList;
        this.context = context;
        this.sloka = sloka;
    }

    @Override
    public RecyclerViewHolder onCreateViewHolder(ViewGroup parent, int viewType) {
        View view = LayoutInflater.from(parent.getContext()).inflate(R.layout.row, null);
        return new RecyclerViewHolder(view, context);
    }

    @Override
    public void onBindViewHolder(RecyclerViewHolder holder, int position) {
        holder.name.setText(itemList.get(position).getName());
        holder.sloka = this.sloka;
        holder.name.setTextColor(context.getResources().getColor(nameColor(itemList.get(position).getDodatek())));
        holder.dodatek = itemList.get(position).getDodatek();
        holder.describe.setText(itemList.get(position).getDescribe());
        holder.plugin.setText(itemList.get(position).getPlugin());
        holder.balik = itemList.get(position).getBalik();
        if (!itemList.get(position).getPlugin().equals("")) {
            holder.plugin.setVisibility(View.VISIBLE);
        } else {
            holder.plugin.setVisibility(View.GONE);
        }
        if (itemList.get(position).getDodatek().equals("life")) {
            holder.describe.setTextColor(context.getResources().getColor(R.color.life));
        } else {
            holder.describe.setTextColor(context.getResources().getColor(describeColor(itemList.get(position).getDescribe())));
        }
        holder.number.setText(itemList.get(position).getNumber());
        if (itemList.get(position).getDodatek().equals("life")) {
            holder.number.setTextColor(context.getResources().getColor(invisibleColor()));
        }

        if (MainScreen.serif) {
            holder.name.setTypeface(Typeface.SERIF);
            holder.describe.setTypeface(Typeface.SERIF);
            holder.number.setTypeface(Typeface.SERIF);
            holder.plugin.setTypeface(Typeface.SERIF);
        }
    }

    @Override
    public int getItemCount() {
        return this.itemList.size();
    }

    public int describeColor(String describe) {
        String[] des = context.getResources().getStringArray(R.array.sections);

        if (des[0].equals(describe)) {
            return R.color.red;
        }
        if (des[1].equals(describe)) {
            return R.color.brown;
        }
        if (des[2].equals(describe)) {
            return R.color.khaki;
        }
        if (des[3].equals(describe)) {
            return R.color.beze;
        }
        if (des[4].equals(describe)) {
            return R.color.darkRed;
        }
        if (des[5].equals(describe)) {
            return R.color.lightPurlpe;
        }
        if (des[6].equals(describe)) {
            return R.color.smooth;
        }
        if (des[7].equals(describe)) {
            return R.color.purlpe;
        }
        if (des[8].equals(describe)) {
            return R.color.golden;
        }
        if (des[9].equals(describe)) {
            return R.color.green;
        }
        if (des[10].equals(describe)) {
            return R.color.lightBlue;
        }
        if (des[11].equals(describe)) {
            return R.color.yellow;
        }
        if (des[12].equals(describe)) {
            return R.color.blue;
        }
        if (des[13].equals(describe)) {
            return R.color.orange;
        }
        return R.color.black;
    }

    public int invisibleColor() {
        if (!MainScreen.night) {
            return R.color.white;
        } else {
            return R.color.black;
        }
    }

    public int nameColor(String name) {
        String dodatek = context.getResources().getString(R.string.HKdodatek);

        if (name.equals("") || name.equals("life")) {
            if (MainScreen.night) {
                return R.color.white;
            } else {
                return R.color.black;
            }
        }

        if (dodatek.equals(name)) {
            return R.color.green;
        }

        dodatek = context.getResources().getString(R.string.OLdodatek);
        if (dodatek.equals(name)) {
            return R.color.lightBlue;
        }

        return R.color.orange;

    }
}

