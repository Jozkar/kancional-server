package jozkar.kancional.server;

import android.content.ClipData;
import android.content.ClipboardManager;
import android.content.DialogInterface;
import android.preference.PreferenceManager;
import android.util.Log;
import android.view.View;
import android.widget.Button;
import android.widget.ImageButton;
import android.widget.RelativeLayout;
import android.widget.TextView;
import android.widget.Toast;

import androidx.appcompat.app.AlertDialog;
import androidx.appcompat.view.ContextThemeWrapper;
import androidx.fragment.app.FragmentActivity;
import androidx.recyclerview.widget.RecyclerView;

import org.json.JSONObject;

import java.net.ConnectException;

import static jozkar.kancional.server.MainScreen.CODE;

/**
 * Created by Josef Ridky on 14-07-2019.
 */
public class PlaylistRVHolder extends RecyclerView.ViewHolder implements View.OnClickListener {

    public TextView name;
    public TextView number;
    public TextView plugin;
    public TextView describe;
    public RelativeLayout row;
    public ImageButton delete;
    public int position;
    public FragmentActivity context;

    public PlaylistRVHolder(View itemView, FragmentActivity c) {
        super(itemView);

        //implementing onClickListener
        position = 0;
        name = (TextView) itemView.findViewById(R.id.name);
        plugin = (TextView) itemView.findViewById(R.id.plugin);
        number = (TextView) itemView.findViewById(R.id.number);
        describe = (TextView) itemView.findViewById(R.id.describe);
        delete = (ImageButton) itemView.findViewById(R.id.remove);
        row = (RelativeLayout) itemView.findViewById(R.id.row);
        delete.setOnClickListener(this::onDeleteClick);
        itemView.setOnClickListener(this);
        context = c;
    }

    public void onDeleteClick(View view) {
        App.playlist.remove(position);
        App.mydb.reorder();
        RemoteFragment.plAdapter.notifyDataSetChanged();
    }

    @Override
    public void onClick(View v){
        if(number.getText().equals("")) {
            ClipData clip = ClipData.newPlainText("name", name.getText());
            App.clipboard.setPrimaryClip(clip);
            Toast.makeText(context, context.getString(R.string.clipboard), Toast.LENGTH_SHORT).show();
        }
    }
}