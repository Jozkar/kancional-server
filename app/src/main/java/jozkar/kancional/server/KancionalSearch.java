package jozkar.kancional.server;

import android.net.nsd.NsdManager;
import android.net.nsd.NsdServiceInfo;
import android.util.Log;

import static jozkar.kancional.server.App.nsdManager;

public class KancionalSearch {
    NsdManager.DiscoveryListener manager;
    NsdManager.ResolveListener resolveListener;
    String TAG = "mDNS", serviceName = "Kancional-Server", SERVICE_TYPE = "_http._tcp.";
    public String IP, PORT;
    public static boolean active = false;


    public KancionalSearch() {
        IP = "";
        resolveListener = new NsdManager.ResolveListener() {

            @Override
            public void onResolveFailed(NsdServiceInfo serviceInfo, int errorCode) {
                // Called when the resolve fails. Use the error code to debug.
                Log.e(TAG, "Resolve failed: " + errorCode);
            }

            @Override
            public void onServiceResolved(NsdServiceInfo serviceInfo) {
                Log.d(TAG, "Resolve Succeeded. " + serviceInfo);

                IP = serviceInfo.getHost().toString();
                PORT = String.valueOf(serviceInfo.getPort());
                Log.d(TAG, "IP " + IP + " PORT " + PORT);
            }
        };

        manager = new NsdManager.DiscoveryListener() {
            // Called as soon as service discovery begins.
            @Override
            public void onDiscoveryStarted(String regType) {
                Log.d(TAG, "Service discovery started");
            }

            @Override
            public void onServiceFound(NsdServiceInfo service) {
                // A service was found! Do something with it.
                Log.d(TAG, "Service discovery success" + service);
                if (!service.getServiceType().equals(SERVICE_TYPE)) {
                    // Service type is the string containing the protocol and
                    // transport layer for this service.
                    Log.d(TAG, "Unknown Service Type: " + service.getServiceType());
                } else if (service.getServiceName().contains(serviceName)) {
                    nsdManager.resolveService(service, resolveListener);
                    active = false;
                } else {
                    nsdManager.stopServiceDiscovery(this);
                    active = false;
                }

            }

            @Override
            public void onServiceLost(NsdServiceInfo service) {
                // When the network service is no longer available.
                // Internal bookkeeping code goes here.
                Log.e(TAG, "service lost: " + service);
                active = false;
            }

            @Override
            public void onDiscoveryStopped(String serviceType) {
                Log.i(TAG, "Discovery stopped: " + serviceType);
                active = false;
            }

            @Override
            public void onStartDiscoveryFailed(String serviceType, int errorCode) {
                Log.e(TAG, "Discovery failed: Error code:" + errorCode);
                nsdManager.stopServiceDiscovery(this);
                active = false;
            }

            @Override
            public void onStopDiscoveryFailed(String serviceType, int errorCode) {
                Log.e(TAG, "Discovery failed: Error code:" + errorCode);
                nsdManager.stopServiceDiscovery(this);
                active = false;
            }
        };
    }

    public void discovery() {
        if(!active) {
            active = true;
            nsdManager.discoverServices("_http._tcp.", NsdManager.PROTOCOL_DNS_SD, manager);
        }
    }

    public void stop() {
        try {
            nsdManager.stopServiceDiscovery(manager);
        }catch (Exception e){
            Log.d(TAG, e.getMessage());
        }
    }
}
