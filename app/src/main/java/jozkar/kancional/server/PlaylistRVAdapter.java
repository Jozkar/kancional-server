package jozkar.kancional.server;

import android.graphics.Color;
import android.graphics.Typeface;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;

import androidx.fragment.app.FragmentActivity;
import androidx.recyclerview.widget.RecyclerView;

import java.util.ArrayList;
import java.util.Collections;
import java.util.List;

public class PlaylistRVAdapter extends RecyclerView.Adapter<PlaylistRVHolder> implements ItemMoveCallback.ItemTouchHelperContract {

    private ArrayList<PlaylistRecord> itemList;
    private FragmentActivity context;

    public PlaylistRVAdapter(FragmentActivity context, ArrayList<PlaylistRecord> itemList) {
        this.itemList = itemList;
        this.context = context;
    }

    @Override
    public PlaylistRVHolder onCreateViewHolder(ViewGroup parent, int viewType) {
        View view = LayoutInflater.from(parent.getContext()).inflate(R.layout.playlist_row, null);
        return new PlaylistRVHolder(view, context);
    }

    @Override
    public void onBindViewHolder(PlaylistRVHolder holder, int position) {
        holder.position = position;
        if(itemList.get(position).getPsalm().equals("")) {
            Record r = App.songs.getByNumber(itemList.get(position).getNumber(), itemList.get(position).getDodatek(), itemList.get(position).getBalik());
            if(r != null) {
                holder.name.setText(r.name);
                holder.name.setTextColor(context.getResources().getColor(nameColor(itemList.get(position).getDodatek())));
                holder.plugin.setText(decodePlugin(itemList.get(position).getBalik()));
                if (!itemList.get(position).getBalik().equals("jozkar.kancional")) {
                    holder.plugin.setVisibility(View.VISIBLE);
                } else {
                    holder.plugin.setVisibility(View.GONE);
                }
                holder.number.setText(itemList.get(position).getNumber());
            } else {
                holder.plugin.setVisibility(View.GONE);
                holder.number.setText(itemList.get(position).getNumber());
                holder.describe.setText("");
                holder.name.setText("");
            }

            if (!itemList.get(position).getVerse().equals("")){
                holder.describe.setText(itemList.get(position).getVerse() + ". sloka");
                holder.describe.setVisibility(View.VISIBLE);
            } else {
                holder.describe.setVisibility(View.GONE);
            }

        } else {
            holder.name.setText(itemList.get(position).getPsalm());
            holder.number.setText("");
            holder.describe.setText("");
            holder.describe.setVisibility(View.GONE);
        }

        if (MainScreen.serif) {
            holder.name.setTypeface(Typeface.SERIF);
            holder.number.setTypeface(Typeface.SERIF);
            holder.describe.setTypeface(Typeface.SERIF);
            holder.plugin.setTypeface(Typeface.SERIF);
        }
    }

    @Override
    public void onRowMoved(int fromPosition, int toPosition) {
        if (fromPosition < toPosition) {
            for (int i = fromPosition; i < toPosition; i++) {
                Collections.swap(itemList, i, i + 1);
            }
        } else {
            for (int i = fromPosition; i > toPosition; i--) {
                Collections.swap(itemList, i, i - 1);
            }
        }
        App.playlist = itemList;
        App.mydb.reorder();
        notifyItemMoved(fromPosition,toPosition);
    }

    @Override
    public void onRowSelected(PlaylistRVHolder myViewHolder) {
        myViewHolder.row.setBackgroundColor(context.getResources().getColor(R.color.blue));
    }

    @Override
    public void onRowClear(PlaylistRVHolder myViewHolder) {
        myViewHolder.row.setBackgroundColor(Color.TRANSPARENT);
        notifyDataSetChanged();
    }

    @Override
    public int getItemCount() {
        return this.itemList.size();
    }

    public int describeColor(String describe) {
        String[] des = context.getResources().getStringArray(R.array.sections);

        if (des[0].equals(describe)) {
            return R.color.red;
        }
        if (des[1].equals(describe)) {
            return R.color.brown;
        }
        if (des[2].equals(describe)) {
            return R.color.khaki;
        }
        if (des[3].equals(describe)) {
            return R.color.beze;
        }
        if (des[4].equals(describe)) {
            return R.color.darkRed;
        }
        if (des[5].equals(describe)) {
            return R.color.lightPurlpe;
        }
        if (des[6].equals(describe)) {
            return R.color.smooth;
        }
        if (des[7].equals(describe)) {
            return R.color.purlpe;
        }
        if (des[8].equals(describe)) {
            return R.color.golden;
        }
        if (des[9].equals(describe)) {
            return R.color.green;
        }
        if (des[10].equals(describe)) {
            return R.color.lightBlue;
        }
        if (des[11].equals(describe)) {
            return R.color.yellow;
        }
        if (des[12].equals(describe)) {
            return R.color.blue;
        }
        if (des[13].equals(describe)) {
            return R.color.orange;
        }
        return R.color.black;
    }

    public String decodePlugin(String plugin){
        String [] plugins = context.getResources().getStringArray(R.array.plugins);
        String [] pluginsTit = context.getResources().getStringArray(R.array.pluginsTit);
        for(int i = 0; i < plugins.length; i++){
            if(plugins[i].equals(plugin)){
                return pluginsTit[i];
            }
        }
        return "";
    }

    public int nameColor(String name) {
        String dodatek = context.getResources().getString(R.string.HKdodatek);

        if (name.equals("") || name.equals("life")) {
            if (MainScreen.night) {
                return R.color.white;
            } else {
                return R.color.black;
            }
        }

        if (dodatek.equals(name)) {
            return R.color.green;
        }

        dodatek = context.getResources().getString(R.string.OLdodatek);
        if (dodatek.equals(name)) {
            return R.color.lightBlue;
        }

        return R.color.orange;

    }
}

