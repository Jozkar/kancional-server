package jozkar.kancional.server;

import android.app.AlertDialog;
import android.content.DialogInterface;
import android.content.Intent;
import android.os.Bundle;
import android.text.Html;
import android.text.Spanned;
import android.text.method.LinkMovementMethod;
import android.util.Log;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.widget.TextView;

import androidx.annotation.NonNull;
import androidx.annotation.Nullable;
import androidx.fragment.app.Fragment;
import androidx.recyclerview.widget.LinearLayoutManager;
import androidx.recyclerview.widget.RecyclerView;

import com.google.android.material.dialog.MaterialAlertDialogBuilder;

import java.text.Normalizer;
import java.util.ArrayList;
import java.util.Collections;
import java.util.List;


public class MainFragment extends Fragment {

    LinearLayoutManager layoutManager;

    public MainFragment() {

    }

    @Nullable
    @Override
    public View onCreateView(@NonNull LayoutInflater inflater, ViewGroup container, Bundle savedInstanceState) {
        View view = inflater.inflate(R.layout.fragment_number_table, container, false);

        int tab = getArguments().getInt("id"),
                type = getArguments().getInt("type");
        String search = getArguments().getString("search");
        String sloka = getArguments().getString("sloka");

        if (sloka.isEmpty()) {
            sloka = "0";
        }

        List<RowData> rowListItem = new ArrayList<>();
        try {
            switch (type) {
                case App.byNumber:
                    rowListItem = getRowList(tab);
                    break;
                case App.byName:
                    rowListItem = getRowListByName(tab);
                    break;
                case App.byGroup:
                    rowListItem = getRowListByGroup(tab);
                    break;
                case App.searchResultNumber:
                    rowListItem = getRowListByNumberSearch(search);
                    break;
                case App.searchResultString:
                    rowListItem = getRowListBySearch(search);
                    break;
                case App.plugin:
                    rowListItem = getRowListByPlugin(tab);
                    break;
                default:
                    rowListItem = new ArrayList<>();
            }
        } catch (Exception e) {
            startActivity(new Intent(getContext(), MainScreen.class));
        }
        layoutManager = new LinearLayoutManager(getActivity());

        RecyclerView recyclerView = (RecyclerView) view.findViewById(R.id.recycler_view);
        recyclerView.setLayoutManager(layoutManager);
        RecyclerViewAdapter adapter = new RecyclerViewAdapter(getActivity(), rowListItem, sloka);
        recyclerView.setAdapter(adapter);

        if (rowListItem.size() < 1) {
            MaterialAlertDialogBuilder dlgAlert = new MaterialAlertDialogBuilder(getContext());
            float dpi = getResources().getDisplayMetrics().density;
            TextView m = new TextView(getContext());
            Spanned msg = Html.fromHtml(getString(R.string.get_plugins));
            m.setText(msg);
            m.setLinkTextColor(getResources().getColor(R.color.link));
            m.setMovementMethod(LinkMovementMethod.getInstance());
            m.setPadding((int) (25 * dpi), (int) (19 * dpi), (int) (25 * dpi), (int) (14 * dpi));
            dlgAlert.setView(m);
            dlgAlert.setTitle(R.string.missing_plugin);
            dlgAlert.setNegativeButton(R.string.ok, new DialogInterface.OnClickListener() {
                @Override
                public void onClick(DialogInterface dialogInterface, int i) {
                    getActivity().finish();
                }
            });
            dlgAlert.setCancelable(false);
            dlgAlert.create().show();
        }

        return view;
    }

    private List<RowData> getRowList(int tab) {

        List<RowData> currentItem = new ArrayList<RowData>();


        try {
            char letter = solveLetter(App.byNumber, tab);
            int start = App.songs.getIndexByNumberFirstLetter(letter);

            if (start < 0) {
                return currentItem;
            }

            for (int i = start; i < App.songs.db.size(); i++) {
                Record r = App.songs.db.get(i);

                if (r.id.charAt(0) == letter) {
                    if (r.balik.equals(getActivity().getPackageName().replace(".server", ""))) {
                        currentItem.add(new RowData(r.id, r.name, r.skupina, r.dodatek, r.plugin, r.balik));
                    }
                } else {
                    break;
                }
            }
        } catch (Exception e) {
            Intent k = new Intent(getContext(), Splash.class);
            startActivity(k);
        }

        return currentItem;
    }


    private List<RowData> getRowListByNumberSearch(String search) {

        List<RowData> currentItem = new ArrayList<RowData>();

        try {

            App.songs.sorting(App.byNumber);
            for (int i = 0; i < App.songs.db.size(); i++) {

                Record r = App.songs.db.get(i);
                if (r.id.contains(search)) {
                    currentItem.add(new RowData(r.id, r.name, r.skupina, r.dodatek, r.plugin, r.balik));
                }
            }
        } catch (Exception e) {
            Intent k = new Intent(getContext(), Splash.class);
            startActivity(k);
        }

        return currentItem;
    }

    private List<RowData> getRowListBySearch(String search) {

        List<RowData> currentItem = new ArrayList<RowData>();

        try {

            String normSearch = Normalizer.normalize(search, Normalizer.Form.NFD);
            normSearch = App.pattern.matcher(normSearch).replaceAll("");

            for (int i = 0; i < App.songs.db.size(); i++) {

                Record r = App.songs.db.get(i);
                if (r.search.matches("(?i:.*" + normSearch + ".*)")) {
                    currentItem.add(new RowData(r.id, r.name, r.skupina, r.dodatek, r.plugin, r.balik));
                }
            }

        } catch (Exception e) {
            Intent k = new Intent(getContext(), Splash.class);
            startActivity(k);
        }

        return currentItem;
    }

    private List<RowData> getRowListByName(int tab) {

        List<RowData> currentItem = new ArrayList<RowData>();

        try {

            char letter = solveLetter(App.byName, tab);
            int start = App.songs.getIndexByNameFirstLetter(letter);

            if (start < 0) {
                return currentItem;
            }

            for (int i = start; i < App.songs.db.size(); i++) {
                Record r = App.songs.db.get(i);

                if (r.name.charAt(0) == letter) {
                    if (r.balik.equals(getActivity().getPackageName().replace(".server", ""))) {
                        currentItem.add(new RowData(r.id, r.name, r.skupina, r.dodatek, r.plugin, r.balik));
                    }
                } else {
                    break;
                }
            }

        } catch (Exception e) {
            Intent k = new Intent(getContext(), Splash.class);
            startActivity(k);
        }

        return currentItem;
    }

    private List<RowData> getRowListByPlugin(int tab) {

        List<RowData> currentItem = new ArrayList<RowData>();

        App.songs.sorting(App.plugin);

        try {
            String plugin = getPlugin(tab);
            int start = App.songs.getIndexByPlugin(plugin);


            if (start < 0) {
                return currentItem;
            }

            for (int i = start; i < App.songs.db.size(); i++) {
                Record r = App.songs.db.get(i);
                if (r.balik.equals(plugin)) {
                    currentItem.add(new RowData(r.id, r.name, r.skupina, r.dodatek, r.plugin, r.balik));
                }
            }
        } catch (Exception e) {
            Intent k = new Intent(getContext(), Splash.class);
            startActivity(k);
        }
        return currentItem;
    }

    private List<RowData> getRowListByGroup(int tab) {

        List<RowData> currentItem = new ArrayList<RowData>();

        try {

            String group = solveString(tab);
            int start = App.songs.getIndexByGroup(group);

            if (start < 0) {
                return currentItem;
            }

            for (int i = start; i < App.songs.db.size(); i++) {
                Record r = App.songs.db.get(i);

                if (r.skupina.equals(group)) {
                    if (r.balik.equals(getActivity().getPackageName().replace(".server", ""))) {
                        currentItem.add(new RowData(r.id, r.name, r.skupina, r.dodatek, r.plugin, r.balik));
                    }
                } else {
                    break;
                }
            }

        } catch (Exception e) {
            Intent k = new Intent(getContext(), Splash.class);
            startActivity(k);
        }
        return currentItem;
    }

    public char solveLetter(int type, int id) {
        String[] letters;
        if (type == App.byNumber) {
            letters = getResources().getStringArray(R.array.group);
        } else {
            letters = getResources().getStringArray(R.array.letters);
        }
        return letters[id].charAt(0);
    }

    public String solveString(int id) {
        String[] section = getResources().getStringArray(R.array.sections);
        return section[id];
    }

    public String getPlugin(int id) {
        ArrayList<String> plugins = new ArrayList<>();
        Collections.addAll(plugins, getContext().getResources().getStringArray(R.array.plugins));
        for (int i = 0, removed = 0; i < App.plugins.length; i++) {
            if (!App.plugins[i]) {
                plugins.remove(i - removed);
                removed++;
            }
        }
        return plugins.size() > 0 ? plugins.get(id) : "";
    }

    @Override
    public void onSaveInstanceState(@NonNull Bundle outState) {

    }
}
