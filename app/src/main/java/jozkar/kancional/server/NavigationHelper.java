package jozkar.kancional.server;

import androidx.fragment.app.Fragment;
import androidx.fragment.app.FragmentManager;

public class NavigationHelper {

    FragmentManager sFM;


    public NavigationHelper(FragmentManager f) {
        this.sFM = f;
    }

    public void navigate(Screen s) {


        Fragment f = getFragment(s);
        replaceFragment(f);
    }

    private Fragment getFragment(Screen s) {
        Fragment f;
        if (s == null) {
            f = new SetupFragment();
            return f;
        }
        switch (s) {
            case HOME:
                f = new SetupFragment();
                break;
            case LISTS:
                f = new ListFragment(true);
                break;
            case SEARCH:
                f = new SearchFragment();
                break;
            case SETTINGS:
                f = new PreferencesFragment();
                break;
            case OTHER:
                f = new ListFragment(false);
                break;
            default:
                throw new IllegalArgumentException("Obrazovka neexistuje");
        }
        return f;
    }

    private void replaceFragment(Fragment f) {
        sFM.beginTransaction().replace(R.id.fragment, f).commit();
    }

    enum Screen {
        HOME,
        SEARCH,
        LISTS,
        SETTINGS,
        OTHER
    }
}
