package jozkar.kancional.server;

import android.content.DialogInterface;
import android.preference.PreferenceManager;
import android.util.Log;
import android.view.View;
import android.widget.TextView;
import android.widget.Toast;

import androidx.appcompat.app.AlertDialog;
import androidx.appcompat.view.ContextThemeWrapper;
import androidx.fragment.app.FragmentActivity;
import androidx.recyclerview.widget.RecyclerView;

import com.google.android.material.dialog.MaterialAlertDialogBuilder;

import org.json.JSONObject;

import java.net.ConnectException;

import static jozkar.kancional.server.MainScreen.CODE;

/**
 * Created by Josef Ridky on 14-07-2019.
 */
public class RecyclerViewHolder extends RecyclerView.ViewHolder implements View.OnClickListener {

    public TextView name;
    public TextView number;
    public TextView describe;
    public TextView plugin;
    public FragmentActivity context;
    public String dodatek, balik, sloka;

    public RecyclerViewHolder(View itemView, FragmentActivity c) {
        super(itemView);

        //implementing onClickListener
        itemView.setOnClickListener(this);
        name = (TextView) itemView.findViewById(R.id.name);
        describe = (TextView) itemView.findViewById(R.id.describe);
        plugin = (TextView) itemView.findViewById(R.id.plugin);
        number = (TextView) itemView.findViewById(R.id.number);
        context = c;
    }

    @Override
    public void onClick(View view) {
        //Every time you click on the row toast is displayed


        try {
            Long tsLong = System.currentTimeMillis() / 1000;

            MainScreen.asyncNumber = number.getText().toString();
            MainScreen.asyncVerse = sloka;
            MainScreen.asyncPsalm = "";
            MainScreen.asyncBalik = balik;
            MainScreen.asyncDodatek = dodatek;
            MainScreen.asyncTime = tsLong.toString();

            if (Table.audic) {
                MainScreen.IP = App.SP.getString("dialIP", "");
                MainScreen.KEY = App.SP.getString("dialKey", "");
                new MainScreen.AsyncPOST().execute();
            }

            if (CODE.equals("")) {
                showAlert();
                return;
            }
            send();
        } catch (Exception e) {
            e.printStackTrace();
        }
        App.SP.edit().putString("currentNumber", number.getText().toString()).putString("currentSloka", sloka).putBoolean("psalm", false).apply();
    }

    private void showAlert() {
        AlertDialog.Builder dlgAlert = new AlertDialog.Builder(context);
        float dpi = context.getResources().getDisplayMetrics().density;
        TextView m = new TextView(context);
        m.setText(context.getString(R.string.wrongCode));
        m.setPadding((int) (25 * dpi), (int) (19 * dpi), (int) (25 * dpi), (int) (14 * dpi));
        dlgAlert.setView(m);
        dlgAlert.setTitle(context.getString(R.string.wrongCodeTitle));
        dlgAlert.setCancelable(false);
        dlgAlert.setPositiveButton(R.string.ok, new DialogInterface.OnClickListener() {
            @Override
            public void onClick(DialogInterface dialog, int which) {
                context.finish();
            }
        });
        dlgAlert.create().show();
    }

    public void showError() {
        try {
            final MaterialAlertDialogBuilder alertBox = new MaterialAlertDialogBuilder(context);
            alertBox.setTitle(context.getString(R.string.wifi));
            alertBox.setMessage(context.getString(R.string.unreachable));
            alertBox.setCancelable(false)
                    .setPositiveButton("Ok", null).create().show();
        } catch (Exception e) {
            Log.d("ERROR", e.getLocalizedMessage());
        }
    }

    public void send() {
        try {
            if (App.ws.isOpen()) {
                JSONObject data = new JSONObject();
                if (!MainScreen.asyncBalik.isEmpty() || !MainScreen.asyncDodatek.isEmpty() || !MainScreen.asyncNumber.isEmpty() || !MainScreen.asyncPsalm.isEmpty()
                        || !MainScreen.asyncVerse.isEmpty() || !MainScreen.asyncTime.isEmpty()) {
                    data.put("time", MainScreen.asyncTime);
                    data.put("source", MainScreen.asyncDodatek);
                    data.put("package", MainScreen.asyncBalik);
                    data.put("song", MainScreen.asyncNumber);
                    data.put("psalm", MainScreen.asyncPsalm);
                    data.put("verse", MainScreen.asyncVerse);
                    data.put("crc", MainScreen.hash(MainScreen.LOCALIP + "|" + MainScreen.CODE));
                    if(MainScreen.camera) {
                        data.put("tv", MainScreen.cameraActive);
                    } else {
                        data.put("tv", true);
                        if(!MainScreen.cameraActive) {
                            MainScreen.cameraActive = true;
                            App.SP.edit().putBoolean("cameraActive", true).apply();
                        }
                    }
                }
                Log.d("JSON", data.toString() + MainScreen.LOCALIP);
                App.ws.sendText(data.toString());
            } else {
                throw new ConnectException("not connected");
            }
            Toast.makeText(context, String.format(context.getString(R.string.setupResult), number.getText()), Toast.LENGTH_SHORT).show();
            context.finish();
        } catch (ConnectException e) {
            showError();
        } catch (Exception e) {
            e.printStackTrace();
        }
    }
}