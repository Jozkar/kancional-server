package jozkar.kancional.server;

import android.content.Context;
import android.os.Bundle;

import androidx.fragment.app.Fragment;
import androidx.fragment.app.FragmentManager;
import androidx.fragment.app.FragmentStatePagerAdapter;

class RemotePagerAdapter extends FragmentStatePagerAdapter {
    Fragment fragment = null;
    Context c;

    public RemotePagerAdapter(FragmentManager fm, Context context) {
        super(fm);
        c = context;
    }

    @Override
    public Fragment getItem(int position) {
        //Based upon the position you can call the fragment you need here
        //here i have called the same fragment for all the instances
        fragment = new RemoteFragment();
        Bundle args = new Bundle();
        args.putInt("id", position);
        fragment.setArguments(args);
        return fragment;
    }


    @Override
    public int getCount() {
        // Returns the number of tabs (If you need 4 tabs change it to 4)
        return 3;
    }

    @Override
    public CharSequence getPageTitle(int position) {
        //this is where you set texts to your tabs based upon the position
        //positions starts from 0
        String[] tabs = c.getResources().getStringArray(R.array.remote);
        return tabs[position];
    }
}