package jozkar.kancional.server;

import android.content.ContentValues;
import android.content.Context;
import android.database.Cursor;
import android.database.sqlite.SQLiteDatabase;
import android.database.sqlite.SQLiteOpenHelper;
import android.util.Log;

import java.util.ArrayList;

/**
 * Created by Jozkar on 20.9.2020.
 */
public class DBHelper extends SQLiteOpenHelper {
    public static final String DB_NAME = "Server.db";
    public static final String TABLE = "playlist";
    public static final String ID = "id", CISLO = "cislo", SLOKA = "sloka", DODATEK = "dodatek", BALIK = "balik", PSALM = "zalm";
    static boolean works = true;


    public DBHelper(Context c) {
        super(c, DB_NAME, null, 1);
    }

    @Override
    public void onCreate(SQLiteDatabase db) {
        db.execSQL("create table " + TABLE + " (" + ID + " integer, " + CISLO + " TEXT, " + SLOKA + " TEXT, " + DODATEK + " TEXT, " + BALIK + " TEXT, " + PSALM + " TEXT)");
    }

    @Override
    public void onUpgrade(SQLiteDatabase db, int oldV, int newV) {
        db.execSQL("DROP TABLE IF EXISTS " + TABLE);
        onCreate(db);
    }

    public void upgrade() {
        SQLiteDatabase db = getWritableDatabase();
        onUpgrade(db, 0, 1);
        db.close();
    }

    public boolean insertRow(int id, String cislo, String sloka, String dodatek, String balik, String psalm) {
        if (works) {
            SQLiteDatabase db = this.getWritableDatabase();
            ContentValues cv = new ContentValues();
            cv.put(ID, id);
            cv.put(DODATEK, dodatek);
            cv.put(BALIK, balik);
            cv.put(CISLO, cislo);
            cv.put(SLOKA, sloka);
            cv.put(PSALM, psalm);
            db.insert(TABLE, null, cv);
        }
        return true;
    }

    public void reorder() {
        if (works) {
            SQLiteDatabase db = this.getWritableDatabase();
            db.execSQL("DELETE FROM " + TABLE);

            for(int i = 0; i < App.playlist.size(); i++){
                insertRow(i, App.playlist.get(i).getNumber(), App.playlist.get(i).getVerse(),App.playlist.get(i).getDodatek(),App.playlist.get(i).getBalik(),App.playlist.get(i).getPsalm());
            }

        }
    }

    public ArrayList<PlaylistRecord> getRows() {
        ArrayList<PlaylistRecord> ar = new ArrayList<PlaylistRecord>();
        try {
            SQLiteDatabase db = this.getReadableDatabase();
            Cursor res = db.rawQuery("select " + ID + ", " + CISLO + ", " + SLOKA + ", " + DODATEK + ", " + PSALM + ", " + BALIK + " from " + TABLE + " ORDER BY " + ID, null);
            res.moveToFirst();
            while (!res.isAfterLast()) {
                Log.d("DB", res.toString());
                PlaylistRecord pr = new PlaylistRecord();
                pr.setDodatek(res.getString(res.getColumnIndex(DODATEK)));
                pr.setNumber(res.getString(res.getColumnIndex(CISLO)));
                pr.setVerse(res.getString(res.getColumnIndex(SLOKA)));
                pr.setBalik(res.getString(res.getColumnIndex(BALIK)));
                pr.setPsalm(res.getString(res.getColumnIndex(PSALM)));
                ar.add(pr);
                res.moveToNext();
            }
            res.close();

            works = true;
        } catch (Exception e) {
            e.printStackTrace();
        }
        return ar;
    }

    @Override
    protected void finalize() throws Throwable {
        this.close();
        super.finalize();
    }
}
