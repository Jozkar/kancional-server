package jozkar.kancional.server;

import android.annotation.TargetApi;
import android.app.AlertDialog;
import android.content.Context;
import android.content.DialogInterface;
import android.content.Intent;
import android.graphics.Typeface;
import android.os.Bundle;
import android.preference.PreferenceManager;
import android.util.Log;
import android.view.KeyEvent;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.view.inputmethod.EditorInfo;
import android.view.inputmethod.InputMethodManager;
import android.widget.ArrayAdapter;
import android.widget.AutoCompleteTextView;
import android.widget.Button;
import android.widget.CheckBox;
import android.widget.EditText;
import android.widget.RelativeLayout;
import android.widget.Switch;
import android.widget.TextView;
import android.widget.Toast;

import androidx.appcompat.view.ContextThemeWrapper;
import androidx.appcompat.widget.SearchView;
import androidx.fragment.app.Fragment;
import androidx.recyclerview.widget.ItemTouchHelper;
import androidx.recyclerview.widget.LinearLayoutManager;
import androidx.recyclerview.widget.RecyclerView;

import com.google.android.material.dialog.MaterialAlertDialogBuilder;
import com.google.android.material.snackbar.Snackbar;

import org.json.JSONObject;

import java.net.ConnectException;
import java.text.Normalizer;
import java.util.ArrayList;
import java.util.Locale;

/**
 * Created by Jozkar on 29.11.2015.
 */
@TargetApi(15)
public class RemoteFragment extends Fragment implements View.OnKeyListener {

    private EditText number, sloka;
    public static SearchView search, dialogSearch;
    public static RecyclerView recyclerView, dialogRecyclerView, plList;

    Button button, slokaPlus, numberPlus, clean, slokaMinus, numberMinus, onOff, plStart, plPlus, plMinus;
    CheckBox camActive;
    private View view, addToPl;
    private PsalmViewAdapter adapter;
    public static PlaylistRVAdapter plAdapter;
    private InputMethodManager imm;
    RecyclerView.LayoutManager layoutManager;

    public RemoteFragment() {
    }

    @Override
    public View onCreateView(LayoutInflater inflater, ViewGroup container, Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);

        int tab = getArguments().getInt("id", 0);

        if (tab < 1) {
            view = inflater.inflate(R.layout.fragment_find_by_number, container, false);

            number = (EditText) view.findViewById(R.id.number);
            sloka = (EditText) view.findViewById(R.id.sloka);
            number.setOnKeyListener(this);
            sloka.setOnKeyListener(this);
            button = (Button) view.findViewById(R.id.set);
            clean = (Button) view.findViewById(R.id.erase);
            slokaPlus = (Button) view.findViewById(R.id.versePlus);
            onOff = (Button) view.findViewById(R.id.onOff);
            slokaMinus = (Button) view.findViewById(R.id.verseMinus);
            plStart = (Button) view.findViewById(R.id.playlistStart);
            plPlus = (Button) view.findViewById(R.id.playlistPlus);
            plMinus = (Button) view.findViewById(R.id.playlistMinus);
            camActive = (CheckBox) view.findViewById(R.id.cameraActive);

            if(MainScreen.showNumber){
                view.findViewById(R.id.buttonGroup2a).setVisibility(View.VISIBLE);
                view.findViewById(R.id.buttonGroup3a).setVisibility(View.VISIBLE);
                numberMinus = (Button) view.findViewById(R.id.numberMinus);
                numberPlus = (Button) view.findViewById(R.id.numberPlus);
            } else {
                view.findViewById(R.id.buttonGroup2a).setVisibility(View.GONE);
                view.findViewById(R.id.buttonGroup3a).setVisibility(View.GONE);
            }

            if (MainScreen.camera) {
                camActive.setVisibility(View.VISIBLE);
                camActive.setChecked(MainScreen.cameraActive);

            } else {
                camActive.setVisibility(View.GONE);
                MainScreen.cameraActive = true;
                App.SP.edit().putBoolean("cameraActive", true).apply();
            }
            imm = (InputMethodManager) getContext().getSystemService(Context.INPUT_METHOD_SERVICE);

            if (MainScreen.serif) {
                button.setTypeface(Typeface.SERIF);
                clean.setTypeface(Typeface.SERIF);
                number.setTypeface(Typeface.SERIF);
                sloka.setTypeface(Typeface.SERIF);
                slokaPlus.setTypeface(Typeface.SERIF);
                onOff.setTypeface(Typeface.SERIF);
                slokaMinus.setTypeface(Typeface.SERIF);
                plMinus.setTypeface(Typeface.SERIF);
                plPlus.setTypeface(Typeface.SERIF);
                plStart.setTypeface(Typeface.SERIF);
                if (MainScreen.camera) {
                    camActive.setTypeface(Typeface.SERIF);
                }
                if (MainScreen.showNumber){
                    numberPlus.setTypeface(Typeface.SERIF);
                    numberMinus.setTypeface(Typeface.SERIF);
                }
            }
            button.setOnClickListener(this::onClick);
            clean.setOnClickListener(this::onClickClean);
            slokaPlus.setOnClickListener(this::onClickSlokaPlus);
            onOff.setOnClickListener(this::onClickOnOff);
            slokaMinus.setOnClickListener(this::onClickSlokaMinus);
            plStart.setOnClickListener(this::onClickPlStart);
            plPlus.setOnClickListener(this::onClickPlPlus);
            plMinus.setOnClickListener(this::onClickPlMinus);
            if (MainScreen.camera) {
                camActive.setOnClickListener(this::onClickTV);
            }
            if (MainScreen.showNumber){
                numberPlus.setOnClickListener(this::onClickNumberPlus);
                numberMinus.setOnClickListener(this::onClickNumberMinus);
            }


        } else if (tab < 2) {
            view = inflater.inflate(R.layout.fragment_psalm, container, false);
            search = (SearchView) view.findViewById(R.id.search);
            layoutManager = new LinearLayoutManager(getActivity());
            recyclerView = (RecyclerView) view.findViewById(R.id.searchResults);
            recyclerView.setLayoutManager(layoutManager);
            ArrayList<String> initial = new ArrayList<String>();
            if (!App.lastPsalm.equals("")) {
                initial.add(App.lastPsalm);
            }
            adapter = new PsalmViewAdapter(getActivity(), initial);

            recyclerView.setAdapter(adapter);

            search.setOnQueryTextListener(new SearchView.OnQueryTextListener() {
                @Override
                public boolean onQueryTextSubmit(String s) {
                    onClickPsalm(search);
                    return false;
                }

                @Override
                public boolean onQueryTextChange(String s) {
                    if (s.isEmpty()) {
                        ArrayList<String> toInitial = new ArrayList<String>();
                        if (!App.lastPsalm.equals("")) {
                            toInitial.add(App.lastPsalm);
                        }
                        adapter = new PsalmViewAdapter(getActivity(), toInitial);
                        recyclerView.setAdapter(adapter);
                    } else {
                        ArrayList<String> searchList = getSearch(s);
                        adapter = new PsalmViewAdapter(getActivity(), searchList);
                        recyclerView.setAdapter(adapter);
                    }
                    return false;
                }
            });

            search.setOnKeyListener(this);

        } else {
            view = inflater.inflate(R.layout.fragment_playlist, container, false);

            imm = (InputMethodManager) getContext().getSystemService(Context.INPUT_METHOD_SERVICE);
            plList = (RecyclerView) view.findViewById(R.id.recycler_view);
            plList.setLayoutManager(new LinearLayoutManager(getContext()));
            plAdapter = new PlaylistRVAdapter(getActivity(), App.playlist);
            ItemTouchHelper.Callback cb = new ItemMoveCallback(plAdapter);
            ItemTouchHelper touchHelper = new ItemTouchHelper(cb);
            touchHelper.attachToRecyclerView(plList);
            plList.setAdapter(plAdapter);

            addToPl = (RelativeLayout) view.findViewById(R.id.addToPlaylist);
            addToPl.setOnClickListener(new View.OnClickListener() {
                @Override
                public void onClick(View v) {
                    View dialog = inflater.inflate(R.layout.playlist_dialog, container, false);

                    ((AutoCompleteTextView) dialog.findViewById(R.id.dialogNumber)).setAdapter(new ArrayAdapter<String>(getContext(), android.R.layout.simple_list_item_1, App.songs.getNumbers()));
                    ((AutoCompleteTextView) dialog.findViewById(R.id.dialogNumber)).setOnKeyListener(RemoteFragment.this::onKey);
                    (dialog.findViewById(R.id.dialogSloka)).setOnKeyListener(RemoteFragment.this::onKey);
                    dialogRecyclerView = (RecyclerView) dialog.findViewById(R.id.dialogSearchResults);
                    dialogRecyclerView.setLayoutManager(new LinearLayoutManager(getContext()));
                    adapter = new PsalmViewAdapter(getActivity(), new ArrayList<String>(), true);
                    dialogRecyclerView.setAdapter(adapter);
                    dialogSearch = (SearchView) dialog.findViewById(R.id.dialogSearch);
                    dialogSearch.setOnKeyListener(RemoteFragment.this::onKey);
                    dialogSearch.setOnQueryTextListener(new SearchView.OnQueryTextListener() {
                        @Override
                        public boolean onQueryTextSubmit(String s) {
                            if (imm != null && imm.isActive()) {
                                imm.hideSoftInputFromWindow(view.getWindowToken(), 0);
                            }
                            return false;
                        }

                        @Override
                        public boolean onQueryTextChange(String s) {
                            Log.d("SEARCH", s);
                            if (s.isEmpty()) {
                                adapter = new PsalmViewAdapter(getActivity(), new ArrayList<String>(), true);
                                dialogRecyclerView.setAdapter(adapter);
                                dialogRecyclerView.invalidate();
                            } else {
                                ArrayList<String> searchList = getSearch(s);
                                adapter = new PsalmViewAdapter(getActivity(), searchList, true);
                                dialogRecyclerView.setAdapter(adapter);
                                dialogRecyclerView.invalidate();
                            }
                            return false;
                        }
                    });


                    final MaterialAlertDialogBuilder alertBox = new MaterialAlertDialogBuilder(getActivity());
                    alertBox.setTitle(getString(R.string.addToPlaylist));
                    alertBox.setView(dialog);
                    alertBox.setCancelable(false)
                            .setPositiveButton(R.string.add, new DialogInterface.OnClickListener() {
                                @Override
                                public void onClick(DialogInterface dialogWindow, int which) {
                                    String number = ((EditText) dialog.findViewById(R.id.dialogNumber)).getText().toString(),
                                            verse = ((EditText) dialog.findViewById(R.id.dialogSloka)).getText().toString(),
                                            psalm = dialogSearch.getQuery().toString(),
                                            dodatek = "", balik = "";

                                    if (!number.equals("")) {
                                        balik = "jozkar.kancional";
                                        if (number.endsWith(" (olomouc)")) {
                                            dodatek = "olomouc";
                                            number = number.replace(" (olomouc)", "");
                                        }

                                        if (number.endsWith(" (hradec)")) {
                                            dodatek = "hradec";
                                            number = number.replace(" (hradec)", "");
                                        }

                                        if (number.endsWith(" (Zpěvníček)")) {
                                            number = number.replace(" (Zpěvníček)", "");
                                            balik = "jozkar.plugin.zpevnicek";
                                            dodatek = "zpevnicek";
                                        }

                                        if (number.endsWith(" (Hlučín)")) {
                                            number = number.replace(" (Hlučín)", "");
                                            balik = "jozkar.plugin.hlucin";
                                            dodatek = "hlucin";
                                        }

                                        if (number.endsWith(" (Pobožnosti)")) {
                                            number = number.replace(" (Pobožnosti)", "");
                                            balik = "jozkar.plugin.poboznosti";
                                            dodatek = "poboznosti";
                                        }

                                        PlaylistRecord pr1 = new PlaylistRecord();
                                        pr1.setNumber(number);
                                        pr1.setVerse(verse);
                                        pr1.setBalik(balik);
                                        pr1.setDodatek(dodatek);
                                        App.playlist.add(pr1);
                                        App.mydb.insertRow(App.playlist.size() - 1, number, verse, dodatek, balik, "");
                                        plAdapter.notifyDataSetChanged();

                                    }

                                    if (!psalm.equals("")) {
                                        PlaylistRecord pr2 = new PlaylistRecord();
                                        pr2.setPsalm(psalm);
                                        App.playlist.add(pr2);
                                        App.mydb.insertRow(App.playlist.size() - 1, "", "", "", "", psalm);
                                        plAdapter.notifyDataSetChanged();
                                    }
                                }
                            })
                            .setNegativeButton(R.string.cancel, null).create().show();
                }
            });
        }

        return view;

    }

    public ArrayList<String> getSearch(String s) {
        ArrayList<String> c = new ArrayList<>();
        String normSearch = Normalizer.normalize(s, Normalizer.Form.NFD);
        normSearch = App.pattern.matcher(normSearch).replaceAll("");

        for (int i = 0; i < App.psalms.size(); i++) {
            String r = Normalizer.normalize(App.psalms.get(i), Normalizer.Form.NFD);
            r = App.pattern.matcher(r).replaceAll("");
            if (r.matches("(?i:" + normSearch + ".*)")) {
                c.add(App.psalms.get(i));
            }
        }

        for (int i = 0; i < App.psalms.size(); i++) {
            String r = Normalizer.normalize(App.psalms.get(i), Normalizer.Form.NFD);
            r = App.pattern.matcher(r).replaceAll("");
            if (r.matches("(?i:.+" + normSearch + ".*)")) {
                c.add(App.psalms.get(i));
            }
        }

        return c;
    }

    @Override
    public boolean onKey(View viewLoc, int i, KeyEvent keyEvent) {
        Log.d("KEYS", i + "");
        switch (viewLoc.getId()) {
            case R.id.number:
                if (i == EditorInfo.IME_ACTION_SEARCH || i == EditorInfo.IME_ACTION_DONE ||
                        keyEvent.getAction() == KeyEvent.ACTION_DOWN && keyEvent.getKeyCode() == KeyEvent.KEYCODE_ENTER) {
                    if (MainScreen.allwaysVerse) {
                        number.requestFocus();
                    } else {
                        button.performClick();
                    }
                }
                break;
            case R.id.sloka:
                if (i == EditorInfo.IME_ACTION_SEARCH || i == EditorInfo.IME_ACTION_DONE ||
                        keyEvent.getAction() == KeyEvent.ACTION_DOWN && keyEvent.getKeyCode() == KeyEvent.KEYCODE_ENTER) {
                    button.performClick();
                    return true;
                }
                break;
            case R.id.search:
                if (i == EditorInfo.IME_ACTION_SEARCH || i == EditorInfo.IME_ACTION_DONE ||
                        keyEvent.getAction() == KeyEvent.ACTION_DOWN && keyEvent.getKeyCode() == KeyEvent.KEYCODE_ENTER) {
                    onClickPsalm(search);
                    return true;
                }
                break;
            case R.id.dialogNumber:
            case R.id.dialogSloka:
            case R.id.dialogSearch:
                if (i == EditorInfo.IME_ACTION_SEARCH || i == EditorInfo.IME_ACTION_DONE || i == EditorInfo.IME_ACTION_GO || i == EditorInfo.IME_ACTION_NEXT || i == 66 ||
                        keyEvent.getAction() == KeyEvent.ACTION_DOWN && keyEvent.getKeyCode() == KeyEvent.KEYCODE_ENTER) {
                    Log.d("IMM", "" + (imm != null && imm.isActive()));
                    if (imm != null && imm.isActive()) {
                        imm.hideSoftInputFromWindow(viewLoc.getWindowToken(), 0);
                    }
                    return true;
                }
                break;

        }
        return false;
    }

    public void onClickTV(View v) {
        MainScreen.cameraActive = camActive.isChecked();
        App.SP.edit().putBoolean("cameraActive", MainScreen.cameraActive).apply();
    }

    public void onClickClean(View v) {
        try {
            if (imm.isActive()) {
                imm.hideSoftInputFromWindow(view.getWindowToken(), 0);
            }

            MainScreen.asyncNumber = "";
            MainScreen.asyncVerse = "";
            MainScreen.asyncPsalm = "";
            MainScreen.asyncBalik = "";
            MainScreen.asyncDodatek = "";
            MainScreen.asyncTime = ((Long) (System.currentTimeMillis() / 1000)).toString();

            if (MainScreen.audic) {
                MainScreen.IP = App.SP.getString("dialIP", "");
                MainScreen.KEY = App.SP.getString("dialKey", "");
                new MainScreen.AsyncPOST().execute();
            }

            if (MainScreen.CODE.equals("")) {
                showAlert();
                return;
            }
            if (send()) {
                Snackbar.make(getContext(),view,String.format(getString(R.string.setupReset)), 1500).show();
                App.SP.edit().putString("currentNumber", "").putString("currentSloka", "").putBoolean("psalm", false).apply();
            }
            number.setText(R.string.empty);
            sloka.setText("");
            return;
        } catch (Exception e) {
            Log.d("EXCEPTION", e.getMessage());

            Intent k = new Intent(getContext(), Splash.class);
            k.putExtra("null", true);
            startActivity(k);
        }
    }

    public void onClickOnOff(View v) {
        try {
            if (imm.isActive()) {
                imm.hideSoftInputFromWindow(view.getWindowToken(), 0);
            }

            MainScreen.asyncNumber = "";
            MainScreen.asyncVerse = "";
            MainScreen.asyncPsalm = "";
            MainScreen.asyncBalik = "";
            MainScreen.asyncDodatek = "";
            if (MainScreen.asyncTime == null || MainScreen.asyncTime.equals("0")) {
                MainScreen.asyncTime = ((Long) (System.currentTimeMillis() / 1000)).toString();
            } else {
                MainScreen.asyncTime = "0";
            }

            if (MainScreen.audic) {
                MainScreen.IP = App.SP.getString("dialIP", "");
                MainScreen.KEY = App.SP.getString("dialKey", "");
                new MainScreen.AsyncPOST().execute();
            }

            if (MainScreen.CODE.equals("")) {
                showAlert();
                return;
            }
            if (send()) {
                Snackbar.make(getContext(), view, String.format(getString((MainScreen.asyncTime.equals("0") ? R.string.setupOff : R.string.setupOn))), 1000).show();
                App.SP.edit().putString("currentNumber", "").putString("currentSloka", "").putBoolean("psalm", false).apply();
            }
            number.setText(R.string.empty);
            sloka.setText("");
            return;
        } catch (Exception e) {
            Log.d("EXCEPTION", e.getMessage());

            Intent k = new Intent(getContext(), Splash.class);
            k.putExtra("null", true);
            startActivity(k);
        }
    }

    public void onClick(View v) {
        String num = number.getText().toString(), slo = sloka.getText().toString();
        int no = -1;

        try {

            if (imm.isActive()) {
                imm.hideSoftInputFromWindow(view.getWindowToken(), 0);
            }
            if (num.length() > 0) {
                no = App.songs.checkNumber(num);
            } else {
                Snackbar.make(getContext(), view, String.format(getString(R.string.emptyNotice)), 1500).show();
                return;
            }

            if (slo.isEmpty()) {
                slo = "0";
            }

            if (MainScreen.secure) {
                if (no > 0) {
                    if (no > 1) {
                        Intent i = new Intent(getContext(), Table.class);
                        i.putExtra("type", App.searchResultNumber);
                        i.putExtra("search", num);
                        i.putExtra("noResults", no);
                        i.putExtra("sloka", slo);
                        startActivity(i);
                    } else {
                        if (num.length() < 3) {
                            num = "0" + num;
                        }

                        String dodatek = App.songs.getByNumber(num).dodatek;
                        String balik = App.songs.getByNumber(num).balik;

                        try {
                            Long tsLong = System.currentTimeMillis() / 1000;

                            MainScreen.asyncNumber = num;
                            MainScreen.asyncVerse = slo;
                            MainScreen.asyncPsalm = "";
                            MainScreen.asyncBalik = balik;
                            MainScreen.asyncDodatek = dodatek;
                            MainScreen.asyncTime = tsLong.toString();

                            if (MainScreen.audic) {
                                MainScreen.IP = App.SP.getString("dialIP", "");
                                MainScreen.KEY = App.SP.getString("dialKey", "");
                                new MainScreen.AsyncPOST().execute();
                            }
                            if (MainScreen.CODE.equals("")) {
                                showAlert();
                                return;
                            }
                            if (send()) {
                                Snackbar.make(getContext(), view, String.format(getString(R.string.setupResult), num), 2000).show();
                                App.SP.edit().putString("currentNumber", num).putString("currentSloka", slo).putBoolean("psalm", false).apply();
                            }
                        } catch (Exception e) {
                            e.printStackTrace();
                        }
                    }
                } else {
                    Snackbar snack = Snackbar.make(getActivity().findViewById(android.R.id.content), getString(R.string.wrongNumber), Snackbar.LENGTH_SHORT);
                    snack.getView().setBackgroundColor(getResources().getColor(R.color.red));
                    snack.show();
                }
            } else {
                try {
                    Long tsLong = System.currentTimeMillis() / 1000;

                    MainScreen.asyncNumber = num;
                    MainScreen.asyncVerse = slo;
                    MainScreen.asyncPsalm = "";
                    MainScreen.asyncBalik = "";
                    MainScreen.asyncDodatek = "";
                    MainScreen.asyncTime = tsLong.toString();


                    if (MainScreen.audic) {
                        MainScreen.IP = App.SP.getString("dialIP", "");
                        MainScreen.KEY = App.SP.getString("dialKey", "");
                        new MainScreen.AsyncPOST().execute();
                    }
                    if (MainScreen.CODE.equals("")) {
                        showAlert();
                        return;
                    }
                    if (send()) {
                        Snackbar.make(getContext(), view, String.format(getString(R.string.setupResult), num), 2000).show();
                        App.SP.edit().putString("currentNumber", num).putString("currentSloka", slo).putBoolean("psalm", false).apply();
                    }
                } catch (Exception e) {
                    e.printStackTrace();
                }
            }


        } catch (Exception e) {
            Log.d("EXCEPTION", e.getMessage());

            Intent k = new Intent(getContext(), Splash.class);
            k.putExtra("null", true);
            startActivity(k);
        }

        number.setText(R.string.empty);
        sloka.setText("");
    }

    public void onClickSlokaPlus(View v) {
        try {

            String num = App.SP.getString("currentNumber", "");
            if (num.equals("")) {
                return;
            }
            try {
                if (MainScreen.asyncVerse == null || MainScreen.asyncVerse.isEmpty()) {
                    MainScreen.asyncVerse = "1";
                } else {
                    MainScreen.asyncVerse = String.valueOf(Integer.parseInt(MainScreen.asyncVerse) + 1);
                }
                MainScreen.asyncTime = String.valueOf(System.currentTimeMillis() / 1000);
                MainScreen.asyncPsalm = "";

                if (MainScreen.audic) {
                    MainScreen.IP = App.SP.getString("dialIP", "");
                    MainScreen.KEY = App.SP.getString("dialKey", "");
                    new MainScreen.AsyncPOST().execute();
                }
                if (MainScreen.CODE.equals("")) {
                    showAlert();
                    return;
                }
                if (send()) {
                    Snackbar.make(getContext(), view, getString(R.string.addSloka), 1000).show();
                    App.SP.edit().putString("currentSloka", String.valueOf(MainScreen.asyncVerse)).apply();
                }
            } catch (Exception e) {
                e.printStackTrace();
                return;
            }
        } catch (Exception e) {
            Log.d("EXCEPTION", e.getMessage());

            Intent k = new Intent(getContext(), Splash.class);
            k.putExtra("null", true);
            startActivity(k);
        }

        number.setText(R.string.empty);
        sloka.setText("");
    }

    public void onClickSlokaMinus(View v) {
        try {

            String num = App.SP.getString("currentNumber", "");
            if (num.equals("")) {
                return;
            }
            try {
                if (MainScreen.asyncVerse == null || MainScreen.asyncVerse.isEmpty() || MainScreen.asyncVerse.equals("0")) {
                    if (MainScreen.asyncVerse.equals("0")) {
                        Snackbar.make(getContext(),view, getString(R.string.unableSloka), 1000).show();
                        return;
                    }
                    MainScreen.asyncVerse = "0";
                } else {
                    MainScreen.asyncVerse = String.valueOf(Integer.parseInt(MainScreen.asyncVerse) - 1);
                }
                MainScreen.asyncTime = String.valueOf(System.currentTimeMillis() / 1000);
                MainScreen.asyncPsalm = "";

                if (MainScreen.audic) {
                    MainScreen.IP = App.SP.getString("dialIP", "");
                    MainScreen.KEY = App.SP.getString("dialKey", "");
                    new MainScreen.AsyncPOST().execute();
                }
                if (MainScreen.CODE.equals("")) {
                    showAlert();
                    return;
                }
                if (send()) {
                    Snackbar.make(getContext(), view, getString(R.string.remSloka), 1000).show();
                    App.SP.edit().putString("currentSloka", String.valueOf(MainScreen.asyncVerse)).apply();
                }
            } catch (Exception e) {
                e.printStackTrace();
                return;
            }
        } catch (Exception e) {
            Log.d("EXCEPTION", e.getMessage());

            Intent k = new Intent(getContext(), Splash.class);
            k.putExtra("null", true);
            startActivity(k);
        }

        number.setText(R.string.empty);
        sloka.setText("");
    }

    public void onClickNumberPlus(View v) {
        try {

            String num = App.SP.getString("currentNumber", "");
            if (num.equals("")) {
                return;
            }
            try {
                if (MainScreen.asyncNumber == null || MainScreen.asyncNumber.isEmpty()) {
                    MainScreen.asyncNumber = "001";
                    MainScreen.asyncBalik = App.songs.getByNumber(MainScreen.asyncNumber).balik;
                    MainScreen.asyncVerse = "0";
                    MainScreen.asyncDodatek = "";
                } else {
                    if (MainScreen.secure) {
                        App.songs.sorting(App.byNumber);
                        int next = App.songs.getIndexByNumber(num, MainScreen.asyncDodatek);
                        Log.d("PLUSNUM", "" + num + " " + MainScreen.asyncDodatek + " " + next + " " + App.songs.checkId(next + 1));
                        if (!App.songs.checkId(next + 1)) {
                            Snackbar.make(getContext(), view, getString(R.string.cantAdd), 2000).show();
                            Log.d("PLUSNUM", "nejde zvysit");
                            return;
                        }
                        MainScreen.asyncNumber = App.songs.db.get(next + 1).id;
                        MainScreen.asyncBalik = App.songs.db.get(next + 1).balik;
                        MainScreen.asyncDodatek = App.songs.db.get(next + 1).dodatek;
                        MainScreen.asyncVerse = "0";
                    } else {
                        int next = Integer.parseInt(MainScreen.asyncNumber.replaceAll("[\\D]", "")) + 1;
                        MainScreen.asyncNumber = String.format(Locale.getDefault(), "%1$03d", next);
                        MainScreen.asyncDodatek = "";
                        MainScreen.asyncVerse = "0";
                        MainScreen.asyncBalik = "";
                    }
                }
                MainScreen.asyncTime = String.valueOf(System.currentTimeMillis() / 1000);
                MainScreen.asyncPsalm = "";

                if (MainScreen.audic) {
                    MainScreen.IP = App.SP.getString("dialIP", "");
                    MainScreen.KEY = App.SP.getString("dialKey", "");
                    new MainScreen.AsyncPOST().execute();
                }
                if (MainScreen.CODE.equals("")) {
                    showAlert();
                    return;
                }
                if (send()) {
                    Snackbar.make(getContext(), view, getString(R.string.addNumber), 1000).show();
                    App.SP.edit().putString("currentNumber", MainScreen.asyncNumber).putString("currentSloka", "0").putBoolean("psalm", false).apply();
                }
                ;
            } catch (Exception e) {
                e.printStackTrace();
                return;
            }
        } catch (Exception e) {
            Log.d("EXCEPTION", e.getMessage());

            Intent k = new Intent(getContext(), Splash.class);
            k.putExtra("null", true);
            startActivity(k);
        }

        number.setText(R.string.empty);
        sloka.setText("");
    }

    public void onClickPlPlus(View v) {

        try {
            if (App.playlist.size() < 1 || App.plPosition < 0) {
                Snackbar.make(view, R.string.menuTextEmpty, 1000).show();
                MainScreen.asyncNumber = "";
                MainScreen.asyncBalik = "";
                MainScreen.asyncVerse = "";
                MainScreen.asyncDodatek = "";
                MainScreen.asyncPsalm = "";
                MainScreen.asyncTime = String.valueOf(System.currentTimeMillis() / 1000);
                App.SP.edit().putString("currentNumber", "").putString("currentSloka", "").putBoolean("psalm", false).apply();
                if (MainScreen.audic) {
                    MainScreen.IP = App.SP.getString("dialIP", "");
                    MainScreen.KEY = App.SP.getString("dialKey", "");
                    new MainScreen.AsyncPOST().execute();
                }
                send();
                return;
            }

            if (App.playlist.size() <= (App.plPosition + 1)) {
                App.SP.edit().putInt("playlist", -1).apply();
                App.plPosition = -1;
                Snackbar.make(view, R.string.menuTextEnd, 1500).show();
            } else {
                App.plPosition++;
                App.SP.edit().putInt("playlist", App.plPosition).apply();
            }

            try {
                PlaylistRecord pr = App.playlist.get(App.plPosition);
                MainScreen.asyncNumber = pr.getNumber();
                MainScreen.asyncBalik = pr.getBalik();
                MainScreen.asyncVerse = pr.getVerse();
                MainScreen.asyncDodatek = pr.getDodatek();
                MainScreen.asyncPsalm = pr.getPsalm();
                MainScreen.asyncTime = String.valueOf(System.currentTimeMillis() / 1000);


                if (MainScreen.audic) {
                    MainScreen.IP = App.SP.getString("dialIP", "");
                    MainScreen.KEY = App.SP.getString("dialKey", "");
                    new MainScreen.AsyncPOST().execute();
                }
                if (MainScreen.CODE.equals("")) {
                    showAlert();
                    return;
                }
                if (send()) {
                    if (pr.getPsalm().equals("")) {
                        if (pr.getVerse().equals("")) {
                            Snackbar.make(getContext(), view, pr.getNumber(), 1000).show();
                        } else {
                            Snackbar.make(getContext(), view, pr.getNumber() + " / " + pr.getVerse(), 1000).show();
                        }
                    } else {
                        Snackbar.make(view, R.string.menuTextPsalm, 1000).show();
                    }
                    App.SP.edit().putString("currentNumber", MainScreen.asyncNumber).putString("currentSloka", MainScreen.asyncVerse).putBoolean("psalm", !pr.getPsalm().equals("")).apply();
                } else {
                    App.plPosition--;
                    App.SP.edit().putInt("playlist", App.plPosition).apply();
                }
            } catch (Exception e) {
                e.printStackTrace();
                return;
            }
        } catch (Exception e) {
            Log.d("EXCEPTION", e.getMessage());

            Intent k = new Intent(getContext(), Splash.class);
            k.putExtra("null", true);
            startActivity(k);
        }
    }

    public void onClickPlMinus(View v) {

        try {
            if (App.playlist.size() < 1 || App.plPosition < 0) {
                Snackbar.make(view, R.string.menuTextEmpty, 1000).show();
                MainScreen.asyncNumber = "";
                MainScreen.asyncBalik = "";
                MainScreen.asyncVerse = "";
                MainScreen.asyncDodatek = "";
                MainScreen.asyncPsalm = "";
                MainScreen.asyncTime = String.valueOf(System.currentTimeMillis() / 1000);
                App.SP.edit().putString("currentNumber", "").putString("currentSloka", "").putBoolean("psalm", false).apply();
                if (MainScreen.audic) {
                    MainScreen.IP = App.SP.getString("dialIP", "");
                    MainScreen.KEY = App.SP.getString("dialKey", "");
                    new MainScreen.AsyncPOST().execute();
                }
                send();
                return;
            }

            if (App.playlist.size() <= (App.plPosition - 1)) {
                App.SP.edit().putInt("playlist", 0).apply();
                App.plPosition = 0;
            } else {
                App.plPosition--;
                App.SP.edit().putInt("playlist", App.plPosition).apply();
            }

            try {
                PlaylistRecord pr = App.playlist.get(App.plPosition);
                MainScreen.asyncNumber = pr.getNumber();
                MainScreen.asyncBalik = pr.getBalik();
                MainScreen.asyncVerse = pr.getVerse();
                MainScreen.asyncDodatek = pr.getDodatek();
                MainScreen.asyncPsalm = pr.getPsalm();
                MainScreen.asyncTime = String.valueOf(System.currentTimeMillis() / 1000);


                if (MainScreen.audic) {
                    MainScreen.IP = App.SP.getString("dialIP", "");
                    MainScreen.KEY = App.SP.getString("dialKey", "");
                    new MainScreen.AsyncPOST().execute();
                }
                if (MainScreen.CODE.equals("")) {
                    showAlert();
                    return;
                }
                if (send()) {
                    if (pr.getPsalm().equals("")) {
                        if (pr.getVerse().equals("")) {
                            Snackbar.make(view, pr.getNumber(), 1000).show();
                        } else {
                            Snackbar.make(view, pr.getNumber() + " / " + pr.getVerse(), 1000).show();
                        }
                    } else {
                        Snackbar.make(view, R.string.menuTextPsalm, 1000).show();
                    }
                    App.SP.edit().putString("currentNumber", MainScreen.asyncNumber).putString("currentSloka", MainScreen.asyncVerse).putBoolean("psalm", !pr.getPsalm().equals("")).apply();
                } else {
                    App.plPosition++;
                    App.SP.edit().putInt("playlist", App.plPosition).apply();
                }
            } catch (Exception e) {
                e.printStackTrace();
                return;
            }
        } catch (Exception e) {
            Log.d("EXCEPTION", e.getMessage());

            Intent k = new Intent(getContext(), Splash.class);
            k.putExtra("null", true);
            startActivity(k);
        }
    }

    public void onClickPlStart(View v) {
        try {
            App.SP.edit().putInt("playlist", 0).apply();
            App.plPosition = 0;
            if (App.playlist.size() < 1) {
                Snackbar.make(view, R.string.menuTextEmpty, 1000).show();
                MainScreen.asyncNumber = "";
                MainScreen.asyncBalik = "";
                MainScreen.asyncVerse = "";
                MainScreen.asyncDodatek = "";
                MainScreen.asyncPsalm = "";
                MainScreen.asyncTime = String.valueOf(System.currentTimeMillis() / 1000);
                App.SP.edit().putString("currentNumber", "").putString("currentSloka", "").putBoolean("psalm", false).apply();
                if (MainScreen.audic) {
                    MainScreen.IP = App.SP.getString("dialIP", "");
                    MainScreen.KEY = App.SP.getString("dialKey", "");
                    new MainScreen.AsyncPOST().execute();
                }
                send();
                return;
            }
            try {
                PlaylistRecord pr = App.playlist.get(0);
                MainScreen.asyncNumber = pr.getNumber();
                MainScreen.asyncBalik = pr.getBalik();
                MainScreen.asyncVerse = pr.getVerse();
                MainScreen.asyncDodatek = pr.getDodatek();
                MainScreen.asyncPsalm = pr.getPsalm();
                MainScreen.asyncTime = String.valueOf(System.currentTimeMillis() / 1000);


                if (MainScreen.audic) {
                    MainScreen.IP = App.SP.getString("dialIP", "");
                    MainScreen.KEY = App.SP.getString("dialKey", "");
                    new MainScreen.AsyncPOST().execute();
                }
                if (MainScreen.CODE.equals("")) {
                    showAlert();
                    return;
                }
                if (send()) {
                    if (pr.getPsalm().equals("")) {
                        if (pr.getVerse().equals("")) {
                            Snackbar.make(view, pr.getNumber(), 1000).show();
                        } else {
                            Snackbar.make(view, pr.getNumber() + " / " + pr.getVerse(), 1000).show();
                        }
                    } else {
                        Snackbar.make(view, R.string.menuTextPsalm, 1000).show();
                    }
                    App.SP.edit().putString("currentNumber", MainScreen.asyncNumber).putString("currentSloka", MainScreen.asyncVerse).putBoolean("psalm", !pr.getPsalm().equals("")).apply();
                } else {
                    App.SP.edit().putInt("playlist", -1).apply();
                    App.plPosition = -1;
                }
            } catch (Exception e) {
                e.printStackTrace();

                return;
            }
        } catch (Exception e) {
            Log.d("EXCEPTION", e.getMessage());

            Intent k = new Intent(getContext(), Splash.class);
            k.putExtra("null", true);
            startActivity(k);
        }
    }

    public void onClickNumberMinus(View v) {
        try {

            String num = App.SP.getString("currentNumber", "");
            if (num.equals("")) {
                return;
            }
            try {
                if (MainScreen.asyncNumber == null || MainScreen.asyncNumber.isEmpty()) {
                    MainScreen.asyncNumber = "001";
                    MainScreen.asyncBalik = App.songs.getByNumber(MainScreen.asyncNumber).balik;
                    MainScreen.asyncVerse = "0";
                    MainScreen.asyncDodatek = "";
                } else {
                    if (MainScreen.secure) {
                        App.songs.sorting(App.byNumber);
                        int next = App.songs.getIndexByNumber(num, MainScreen.asyncDodatek);
                        if (!App.songs.checkId(next - 1)) {
                            Snackbar.make(view, getString(R.string.unableNumber), 1000).show();
                            return;
                        }
                        MainScreen.asyncNumber = App.songs.db.get(next - 1).id;
                        MainScreen.asyncBalik = App.songs.db.get(next - 1).balik;
                        MainScreen.asyncDodatek = App.songs.db.get(next - 1).dodatek;
                        MainScreen.asyncVerse = "0";
                    } else {
                        int next = Integer.parseInt(MainScreen.asyncNumber.replaceAll("[\\D]", "")) - 1;
                        if (next == 0) {
                            Snackbar.make(view, getString(R.string.unableNumber), 1000).show();
                            return;
                        }
                        MainScreen.asyncNumber = String.format(Locale.getDefault(), "%1$03d", next);
                        MainScreen.asyncDodatek = "";
                        MainScreen.asyncVerse = "0";
                        MainScreen.asyncBalik = "";
                    }
                }
                MainScreen.asyncTime = String.valueOf(System.currentTimeMillis() / 1000);
                MainScreen.asyncPsalm = "";

                if (MainScreen.audic) {
                    MainScreen.IP = App.SP.getString("dialIP", "");
                    MainScreen.KEY = App.SP.getString("dialKey", "");
                    new MainScreen.AsyncPOST().execute();
                }
                if (MainScreen.CODE.equals("")) {
                    showAlert();
                    return;
                }
                if (send()) {
                    Snackbar.make(view, getString(R.string.remNumber), 1000).show();
                    App.SP.edit().putString("currentNumber", MainScreen.asyncNumber).putString("currentSloka", "0").putBoolean("psalm", false).apply();
                }
            } catch (Exception e) {
                e.printStackTrace();
                return;
            }
        } catch (Exception e) {
            Log.d("EXCEPTION", e.getMessage());

            Intent k = new Intent(getContext(), Splash.class);
            k.putExtra("null", true);
            startActivity(k);
        }

        number.setText(R.string.empty);
        sloka.setText("");
    }

    public void onClickPsalm(View v) {

        String psalm = search.getQuery().toString();
        App.lastPsalm = psalm;
        App.SP.edit().putString("lastPsalm", App.lastPsalm).apply();

        try {


            Long tsLong = System.currentTimeMillis() / 1000;

            MainScreen.asyncNumber = "";
            MainScreen.asyncVerse = "";
            MainScreen.asyncPsalm = psalm;
            MainScreen.asyncBalik = "";
            MainScreen.asyncDodatek = "";
            MainScreen.asyncTime = tsLong.toString();


            if (MainScreen.audic) {
                MainScreen.IP = App.SP.getString("dialIP", "");
                MainScreen.KEY = App.SP.getString("dialKey", "");
                new MainScreen.AsyncPOST().execute();
            }

            if (MainScreen.CODE.equals("")) {
                showAlert();
            } else {
                if (send()) {
                    Snackbar.make(view, getString(R.string.psalmResult), 1000).show();
                    App.SP.edit().putString("currentNumber", "").putString("currentSloka", "").putBoolean("psalm", true).apply();
                }
            }

        } catch (Exception e) {
            Intent k = new Intent(getContext(), Splash.class);
            startActivity(k);
        }

        search.setQuery("", false);
    }

    private void showAlert() {
        MaterialAlertDialogBuilder dlgAlert = new MaterialAlertDialogBuilder(getContext());
        float dpi = getResources().getDisplayMetrics().density;
        TextView m = new TextView(getContext());
        m.setTextColor(getResources().getColor(MainScreen.night ? R.color.white : R.color.black));
        m.setText(getString(R.string.wrongCode));
        m.setPadding((int) (25 * dpi), (int) (19 * dpi), (int) (25 * dpi), (int) (14 * dpi));
        dlgAlert.setView(m);
        dlgAlert.setTitle(getString(R.string.wrongCodeTitle));
        dlgAlert.setCancelable(false);
        dlgAlert.setPositiveButton(R.string.ok, null);
        dlgAlert.create().show();
    }

    public void showError() {
        final MaterialAlertDialogBuilder alertBox = new MaterialAlertDialogBuilder(getContext());
        alertBox.setTitle(getString(R.string.wifi));
        alertBox.setMessage(getString(R.string.unreachable));
        alertBox.setCancelable(false)
                .setPositiveButton("Ok", null).create().show();
    }

    public boolean send() {
        try {
            if (App.ws.isOpen()) {
                JSONObject data = new JSONObject();
                if (!MainScreen.asyncBalik.isEmpty() || !MainScreen.asyncDodatek.isEmpty() || !MainScreen.asyncNumber.isEmpty() || !MainScreen.asyncPsalm.isEmpty()
                        || !MainScreen.asyncVerse.isEmpty() || !MainScreen.asyncTime.isEmpty()) {
                    data.put("time", MainScreen.asyncTime);
                    data.put("source", MainScreen.asyncDodatek);
                    data.put("package", MainScreen.asyncBalik);
                    data.put("song", MainScreen.asyncNumber);
                    data.put("psalm", MainScreen.asyncPsalm);
                    data.put("verse", MainScreen.asyncVerse);
                    data.put("crc", MainScreen.hash(MainScreen.LOCALIP + "|" + MainScreen.CODE));
                    if (MainScreen.camera) {
                        data.put("tv", MainScreen.cameraActive);
                    } else {
                        data.put("tv", true);
                        if (!MainScreen.cameraActive) {
                            MainScreen.cameraActive = true;
                            App.SP.edit().putBoolean("cameraActive", true).apply();
                        }
                    }
                }
                Log.d("JSON", data.toString() + MainScreen.LOCALIP);
                App.ws.sendText(data.toString());
                return true;
            } else {
                throw new ConnectException("not connected");
            }
        } catch (ConnectException e) {
            showError();
        } catch (Exception e) {
            e.printStackTrace();
        }
        return false;
    }
}
