package jozkar.kancional.server;

import android.app.AlertDialog;
import android.app.NotificationManager;
import android.content.Context;
import android.content.DialogInterface;
import android.content.Intent;
import android.media.AudioManager;
import android.net.ConnectivityManager;
import android.net.Network;
import android.net.NetworkCapabilities;
import android.net.nsd.NsdManager;
import android.net.wifi.WifiManager;
import android.os.AsyncTask;
import android.os.Build;
import android.os.Bundle;
import android.os.PowerManager;
import android.provider.Settings;
import android.text.format.Formatter;
import android.util.Log;
import android.view.Window;
import android.view.WindowManager;
import android.widget.TextView;

import androidx.appcompat.app.AppCompatActivity;

import com.google.android.material.dialog.MaterialAlertDialogBuilder;

import org.json.JSONArray;
import org.json.JSONObject;

import java.io.OutputStreamWriter;
import java.net.HttpURLConnection;
import java.net.URL;
import java.security.MessageDigest;
import java.util.Timer;

import static jozkar.kancional.server.App.SP;

public class AppCompatWrapper extends AppCompatActivity {

    public static boolean night, serif, initialized, secure, rewriteIndex, audic, fullscreen, allwaysVerse, camera, cameraActive, showNumber;
    public static String asyncNumber, asyncVerse, asyncPsalm, asyncBalik, asyncDodatek, asyncTime, IP, KEY, CODE, LOCALIP;
    public static AudioManager amanager;
    public static int old = -1, version;
    public static Timer timer;

    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);

        version = SP.getInt("oldVersion", 0);

        night = SP.getBoolean("night", false);
        camera = SP.getBoolean("camera", false);
        showNumber = SP.getBoolean("showNumber", false);
        cameraActive = SP.getBoolean("cameraActive", true);
        serif = SP.getBoolean("serif", false);
        secure = SP.getBoolean("secure", true);
        allwaysVerse = SP.getBoolean("allwaysVerse", false);
        initialized = SP.getBoolean("initialized", false);
        rewriteIndex = SP.getBoolean("rewrite", false);
        audic = SP.getBoolean("dial", false);
        fullscreen = SP.getBoolean("fullscreen", true);
        CODE = SP.getString("code", "");
        old = SP.getInt("oldAudio", -1);

        if (night) {
            setTheme(R.style.Dark);
        }

        if (fullscreen) {
            requestWindowFeature(Window.FEATURE_NO_TITLE);
            this.getWindow().setFlags(WindowManager.LayoutParams.FLAG_FULLSCREEN, WindowManager.LayoutParams.FLAG_FULLSCREEN);
        }

        getWindow().addFlags(WindowManager.LayoutParams.FLAG_KEEP_SCREEN_ON);

        amanager = (AudioManager) getSystemService(Context.AUDIO_SERVICE);
        App.nsdManager = (NsdManager) getSystemService(Context.NSD_SERVICE);

        if (!getIntent().getBooleanExtra("DHCP", true)) {
            MaterialAlertDialogBuilder alrt = getAlert(getString(R.string.wifiAddress), getString(R.string.wifi));
            alrt.setPositiveButton(R.string.ok, new DialogInterface.OnClickListener() {
                @Override
                public void onClick(DialogInterface dialog, int which) {
                    startActivity(new Intent(Settings.ACTION_WIFI_SETTINGS));
                }
            });
            alrt.create().show();
        }
    }

    public static String hash(String data) {
        byte[] hash;

        try {
            hash = MessageDigest.getInstance("MD5").digest(data.getBytes("UTF-8"));
        } catch (Exception e) {
            throw new RuntimeException("Huh, UTF-8 should be supported?", e);
        }

        StringBuilder hex = new StringBuilder(hash.length * 2);
        for (byte b : hash) {
            if ((b & 0xFF) < 0x10) {
                hex.append("0");
            }
            hex.append(Integer.toHexString(b & 0xFF));
        }
        return hex.toString();

    }

    public MaterialAlertDialogBuilder getAlert(String text, String title) {
        MaterialAlertDialogBuilder dlgAlert = new MaterialAlertDialogBuilder(this);
        float dpi = getResources().getDisplayMetrics().density;
        TextView m = new TextView(this);
        m.setText(text);
        m.setPadding((int) (25 * dpi), (int) (19 * dpi), (int) (25 * dpi), (int) (14 * dpi));
        dlgAlert.setView(m);
        dlgAlert.setTitle(title);
        dlgAlert.setCancelable(false);
        dlgAlert.setNeutralButton(R.string.cancel, null);
        return dlgAlert;
    }

    public void setSound() {

        NotificationManager nm = (NotificationManager) getSystemService(NOTIFICATION_SERVICE);
        if (Build.VERSION.SDK_INT >= Build.VERSION_CODES.M && !nm.isNotificationPolicyAccessGranted()) {
            Intent nmIntent = new Intent(Settings.ACTION_NOTIFICATION_POLICY_ACCESS_SETTINGS);
            startActivity(nmIntent);
        } else {
            if (old == -1) {
                old = amanager.getRingerMode();
                SP.edit().putInt("oldAudio", old).apply();
            }
            amanager.setRingerMode(AudioManager.RINGER_MODE_SILENT);
        }
    }

    @Override
    protected void onResume() {
        SP.edit().putBoolean("myActivity", true).apply();
        setSound();
        super.onResume();
    }

    @Override
    protected void onStop() {
        if (timer != null) {
            timer.cancel();
        }

        PowerManager pm = (PowerManager) getSystemService(Context.POWER_SERVICE);
        if (Build.VERSION.SDK_INT >= Build.VERSION_CODES.KITKAT_WATCH) {
            if (pm != null && !pm.isInteractive()) {
                SP.edit().putBoolean("myActivity", true).apply();
            }
        } else {
            if (pm != null && !pm.isScreenOn()) {
                SP.edit().putBoolean("myActivity", true).apply();
            }
        }

        if (old != -1 && !SP.getBoolean("myActivity", false)) {
            amanager.setRingerMode(old);
            old = -1;
            SP.edit().putInt("oldAudio", -1).putInt("oldAlarm", -1).apply();
            Log.d("SOUND", "RESTORED");
        }

        SP.edit().putBoolean("myActivity", false).apply();
        super.onStop();
    }

    @Override
    protected void onStart() {
        checkWifi();
        super.onStart();
    }

    @Override
    protected void onRestart() {
        checkWifi();
        super.onRestart();
    }

    @Override
    protected void onPause() {
        if (SP.getBoolean("myActivity", false)) {
            SP.edit().putBoolean("myActivity", false).apply();
        }
        super.onPause();
    }

    public void checkWifi() {
        WifiManager wifiManager = (WifiManager) getApplicationContext().getSystemService(Context.WIFI_SERVICE);
        ConnectivityManager tm = (ConnectivityManager) getApplicationContext().getSystemService(Context.CONNECTIVITY_SERVICE);
        if (tm != null) {
            if (Build.VERSION.SDK_INT >= Build.VERSION_CODES.Q) {
                Network[] networks = tm.getAllNetworks();
                boolean wifi = false;
                for (Network network : networks) {
                    NetworkCapabilities capabilities = tm.getNetworkCapabilities(network);
                    if (capabilities != null && capabilities.hasTransport(NetworkCapabilities.TRANSPORT_CELLULAR)) {
                        if(tm.getNetworkInfo(ConnectivityManager.TYPE_MOBILE) != null && tm.getNetworkInfo(ConnectivityManager.TYPE_MOBILE).isConnected()) {
                            MaterialAlertDialogBuilder dlgAlert = getAlert(getString(R.string.mobileText), getString(R.string.mobile));
                            dlgAlert.setPositiveButton(R.string.ok, new DialogInterface.OnClickListener() {
                                @Override
                                public void onClick(DialogInterface dialog, int which) {
                                    dialog.dismiss();
                                    Intent panelIntent = new Intent(Settings.Panel.ACTION_INTERNET_CONNECTIVITY);
                                    startActivityForResult(panelIntent, 0);
                                }
                            });
                            dlgAlert.create().show();
                            break;
                        }
                    }
                    if (capabilities != null && capabilities.hasTransport(NetworkCapabilities.TRANSPORT_WIFI)) {
                        wifi = true;
                    }
                }

                if (!wifi) {
                    MaterialAlertDialogBuilder dlgAlert = getAlert(getString(R.string.wifiText), getString(R.string.wifi));
                    dlgAlert.setPositiveButton(R.string.ok, new DialogInterface.OnClickListener() {
                        @Override
                        public void onClick(DialogInterface dialog, int which) {
                            dialog.dismiss();
                            Intent panelIntent = new Intent(Settings.Panel.ACTION_INTERNET_CONNECTIVITY);
                            startActivityForResult(panelIntent, 0);
                        }
                    });
                    dlgAlert.create().show();
                }
            } else if (Build.VERSION.SDK_INT >= Build.VERSION_CODES.M) {
                Network[] networks = tm.getAllNetworks();
                boolean wifi = false;
                for (Network network : networks) {
                    NetworkCapabilities capabilities = tm.getNetworkCapabilities(network);
                    if (capabilities != null && capabilities.hasTransport(NetworkCapabilities.TRANSPORT_CELLULAR)) {
                        if(tm.getNetworkInfo(ConnectivityManager.TYPE_MOBILE) != null && tm.getNetworkInfo(ConnectivityManager.TYPE_MOBILE).isConnected()){
                            MaterialAlertDialogBuilder dlgAlert = getAlert(getString(R.string.mobileText), getString(R.string.mobile));
                            dlgAlert.setPositiveButton(R.string.ok, new DialogInterface.OnClickListener() {
                                @Override
                                public void onClick(DialogInterface dialog, int which) {
                                    dialog.dismiss();
                                    Intent intent = new Intent(Settings.ACTION_WIRELESS_SETTINGS);
                                    startActivity(intent);
                                }
                            });
                            dlgAlert.create().show();
                            break;
                        }
                    }
                    if (capabilities != null && capabilities.hasTransport(NetworkCapabilities.TRANSPORT_WIFI)) {
                        wifi = true;
                    }
                }
                if (!wifi) {
                    MaterialAlertDialogBuilder dlgAlert = getAlert(getString(R.string.wifiText), getString(R.string.wifi));
                    dlgAlert.setPositiveButton(R.string.ok, new DialogInterface.OnClickListener() {
                        @Override
                        public void onClick(DialogInterface dialog, int which) {
                            dialog.dismiss();
                            Intent intent = new Intent(Settings.ACTION_WIRELESS_SETTINGS);
                            startActivity(intent);
                        }
                    });
                    dlgAlert.create().show();
                }
            } else {
                if (tm.getNetworkInfo(ConnectivityManager.TYPE_MOBILE) != null && tm.getNetworkInfo(ConnectivityManager.TYPE_MOBILE).isConnected()) {
                    MaterialAlertDialogBuilder dlgAlert = getAlert(getString(R.string.mobileText), getString(R.string.mobile));
                    dlgAlert.setPositiveButton(R.string.ok, new DialogInterface.OnClickListener() {
                        @Override
                        public void onClick(DialogInterface dialog, int which) {
                            dialog.dismiss();
                            Intent intent = new Intent(Settings.ACTION_WIRELESS_SETTINGS);
                            startActivity(intent);
                        }
                    });
                    dlgAlert.create().show();
                }
                if (wifiManager != null && !wifiManager.isWifiEnabled()) {
                    wifiManager.setWifiEnabled(true);
                }
            }
        }
        if (wifiManager != null) {
            Thread timerThread = new Thread() {
                public void run() {
                    while (wifiManager.getWifiState() != WifiManager.WIFI_STATE_ENABLED) ;
                    while (wifiManager.getConnectionInfo().getIpAddress() == 0) ;
                    while (Formatter.formatIpAddress(wifiManager.getConnectionInfo().getIpAddress()) == null) ;
                    LOCALIP = Formatter.formatIpAddress(wifiManager.getConnectionInfo().getIpAddress());
                }
            };
            timerThread.start();
        }
    }

    public static class AsyncPOST extends AsyncTask<Void, Void, Void> {

        @Override
        protected Void doInBackground(Void... voids) {

            try {
                URL url = new URL("http://" + IP);
                HttpURLConnection hc = (HttpURLConnection) url.openConnection();
                hc.setRequestMethod("POST");
                hc.setRequestProperty("Accept", "application/json");
                hc.setRequestProperty("Content-Type", "application/json");
                hc.setDoOutput(true);

                OutputStreamWriter ow = new OutputStreamWriter(hc.getOutputStream());
                JSONObject audicData = new JSONObject();
                if (asyncPsalm.isEmpty()) {
                    if (!asyncNumber.isEmpty()) {
                        audicData.put("songNo", Integer.parseInt(asyncNumber.replaceAll("\\D", "")));
                        if (!asyncVerse.equals("0")) {
                            audicData.put("verse", asyncVerse);
                        }
                    }
                } else {
                    audicData.put("psalm", asyncPsalm);
                }
                audicData.put("accessKeys", new JSONArray().put(MainScreen.hash(KEY)));
                Log.d("JSON", audicData.toString());
                ow.write(audicData.toString());
                ow.flush();
                ow.close();
                hc.disconnect();
            } catch (Exception e) {
                e.printStackTrace();
            }
            return null;
        }
    }
}
