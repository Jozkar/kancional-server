package jozkar.kancional.server;

import android.content.Intent;
import android.view.View;
import android.widget.ImageView;
import android.widget.TextView;

import androidx.fragment.app.FragmentActivity;
import androidx.recyclerview.widget.RecyclerView;

/**
 * Created by Jozkar on 14.11.2015.
 */
public class MainRecyclerViewHolder extends RecyclerView.ViewHolder implements View.OnClickListener {

    public TextView name;
    public ImageView number;
    public String id;
    int position;
    public FragmentActivity context;

    public MainRecyclerViewHolder(View itemView, FragmentActivity c) {
        super(itemView);

        //implementing onClickListener
        itemView.setOnClickListener(this);
        name = (TextView) itemView.findViewById(R.id.name);
        number = (ImageView) itemView.findViewById(R.id.number);
        context = c;
    }

    @Override
    public void onClick(View view) {
        //Every time you click on the row toast is displayed

        Intent i;
        try {
            switch (position) {
                case 1:
                    App.songs.sorting(App.plugin);
                    i = new Intent(context, Table.class);
                    i.putExtra("type", App.byNumber);
                    context.startActivity(i);
                    break;
                case 2:
                    App.songs.sorting(App.byName);
                    i = new Intent(context, Table.class);
                    i.putExtra("type", App.byName);
                    context.startActivity(i);
                    break;
                case 3:
                    App.songs.sorting(App.byGroup);
                    i = new Intent(context, Table.class);
                    i.putExtra("type", App.byGroup);
                    context.startActivity(i);
                    break;
                case 4:
                    i = new Intent(context, Table.class);
                    i.putExtra("type", App.plugin);
                    context.startActivity(i);
                    break;
                case 5:
                    i = new Intent(context, About.class);
                    i.putExtra("type", 0);
                    context.startActivity(i);
                    break;
                case 6:
                    i = new Intent(context, About.class);
                    i.putExtra("type", 1);
                    context.startActivity(i);
                    break;
                default:
            }
        } catch (NullPointerException e) {
            Intent k = new Intent(context, Splash.class);
            context.startActivity(k);
        }

    }
}
