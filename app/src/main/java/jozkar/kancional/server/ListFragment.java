package jozkar.kancional.server;

import android.content.res.Configuration;
import android.os.Build;
import android.os.Bundle;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;

import androidx.annotation.NonNull;
import androidx.annotation.Nullable;
import androidx.fragment.app.Fragment;
import androidx.recyclerview.widget.GridLayoutManager;
import androidx.recyclerview.widget.LinearLayoutManager;
import androidx.recyclerview.widget.RecyclerView;

import java.util.ArrayList;
import java.util.List;

public class ListFragment extends Fragment {

    public View view;
    boolean lists;

    public ListFragment() {
    }

    public ListFragment(boolean type) {
        this.lists = type;
    }

    @Override
    public View onCreateView(LayoutInflater inflater, ViewGroup container, @Nullable Bundle savedInstanceState) {
        super.onViewCreated(view, savedInstanceState);

        view = inflater.inflate(R.layout.fragment_number_table, container, false);
        List<MainRowData> rowListItem;
        if (this.lists) {
            rowListItem = getSongsList();
        } else {
            rowListItem = getOtherList();
        }

        RecyclerView.LayoutManager layoutManager;
        if (getResources().getConfiguration().orientation == Configuration.ORIENTATION_LANDSCAPE) {
            layoutManager = new GridLayoutManager(getContext(), 3);
        } else {
            if (getResources().getConfiguration().smallestScreenWidthDp >= 600) {
                layoutManager = new GridLayoutManager(getContext(), 2);
            } else {
                layoutManager = new LinearLayoutManager(getContext());
            }
        }

        RecyclerView recyclerView = (RecyclerView) view.findViewById(R.id.recycler_view);
        recyclerView.setLayoutManager(layoutManager);

        MainRecyclerViewAdapter adapter = new MainRecyclerViewAdapter(getActivity(), rowListItem, getActivity().getPackageName());
        recyclerView.setAdapter(adapter);
        return view;
    }

    private List<MainRowData> getSongsList() {

        List<MainRowData> currentItem = new ArrayList<MainRowData>();

        currentItem.add(new MainRowData(1, getString(R.string.chooseNumber), R.mipmap.number));
        currentItem.add(new MainRowData(2, getString(R.string.chooseLetter), R.mipmap.alphabet));
        currentItem.add(new MainRowData(3, getString(R.string.chooseGroup), R.mipmap.group));
        currentItem.add(new MainRowData(4, getString(R.string.action_plugins), R.mipmap.plugin));

        return currentItem;
    }

    private List<MainRowData> getOtherList() {

        List<MainRowData> currentItem = new ArrayList<MainRowData>();

        currentItem.add(new MainRowData(5, getString(R.string.info_project), (Build.VERSION.SDK_INT >= Build.VERSION_CODES.O ? R.mipmap.ic_launcher_menu : R.mipmap.ic_launcher)));
        currentItem.add(new MainRowData(6, getString(R.string.info_app), R.mipmap.info));

        return currentItem;
    }

    @Override
    public void onSaveInstanceState(@NonNull Bundle outState) {

    }
}
