package jozkar.kancional.server;

import android.app.AlertDialog;
import android.content.Context;
import android.content.DialogInterface;
import android.content.Intent;
import android.net.ConnectivityManager;
import android.net.wifi.WifiManager;
import android.os.Bundle;
import android.provider.Settings;
import android.util.Log;
import android.widget.TextView;

import androidx.appcompat.app.AppCompatActivity;

import com.google.android.material.dialog.MaterialAlertDialogBuilder;

/**
 * Created by Jozkar on 4.4.2016.
 */
public class Splash extends AppCompatActivity {

    static WifiManager wifiManager;
    static ConnectivityManager tm;

    @Override
    protected void onStart() {
        if (tm.getNetworkInfo(ConnectivityManager.TYPE_MOBILE).isConnected()) {
            MaterialAlertDialogBuilder dlgAlert = new MaterialAlertDialogBuilder(this);
            float dpi = getResources().getDisplayMetrics().density;
            TextView m = new TextView(this);
            m.setText(getString(R.string.mobileText));
            m.setPadding((int) (25 * dpi), (int) (19 * dpi), (int) (25 * dpi), (int) (14 * dpi));
            dlgAlert.setView(m);
            dlgAlert.setTitle(getString(R.string.mobile));
            dlgAlert.setNeutralButton(R.string.cancel, new DialogInterface.OnClickListener() {
                @Override
                public void onClick(DialogInterface dialog, int which) {
                    Intent intent = new Intent(Settings.ACTION_WIRELESS_SETTINGS);
                    startActivity(intent);
                }
            });
            dlgAlert.setPositiveButton(R.string.ok, new DialogInterface.OnClickListener() {
                @Override
                public void onClick(DialogInterface dialog, int which) {
                    Intent intent = new Intent(Settings.ACTION_WIRELESS_SETTINGS);
                    startActivity(intent);
                }
            });
            dlgAlert.setCancelable(false);
            dlgAlert.create().show();
        } else {
            Thread timerThread = new Thread() {
                public void run() {
                    checkWifi();
                    Log.d("WIFI", "checked");
                    try {
                        sleep(1000);
                    } catch (Exception e) {
                        Log.d("SLEEP", "Sleep exception");
                    }
                    checkDHCP();
                    Intent i = new Intent(Splash.this, MainScreen.class);
                    i.putExtra("DHCP", true);
                    startActivity(i);
                    finish();
                }
            };
            timerThread.start();
        }
        super.onStart();
    }

    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.splash);
        wifiManager = (WifiManager) getApplicationContext().getSystemService(Context.WIFI_SERVICE);
        tm = (ConnectivityManager) getApplicationContext().getSystemService(Context.CONNECTIVITY_SERVICE);
    }

    public void checkDHCP() {
        if (wifiManager.getDhcpInfo().ipAddress == 0) {
            Intent i = new Intent(Splash.this, MainScreen.class);
            i.putExtra("DHCP", false);
            startActivity(i);
            finish();
        }
        while (wifiManager.getWifiState() != WifiManager.WIFI_STATE_ENABLED) ;
    }

    public void checkWifi() {
        if (!wifiManager.isWifiEnabled()) {
            wifiManager.setWifiEnabled(true);
        }
        while (wifiManager.getWifiState() != WifiManager.WIFI_STATE_ENABLED) ;
    }
}
